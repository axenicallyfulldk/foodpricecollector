from food_parser.core import Snapshot

class SnapshotRepo:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = conn.cursor()

    getByIdReq = "select id, date from snapshot where id = %s"
    def getById(self, id):
        self.cursor.execute(SnapshotRepo.getByIdReq, (id,))
        record = self.cursor.fetchone()

        if record is None:
            return None

        return Snapshot(record[0], record[1])

    getByDateReq = "select id, date from snapshot where date = %s"
    def getByDate(self, date):
        self.cursor.execute(SnapshotRepo.getByDateReq, (date,))
        record = self.cursor.fetchone()

        if record is None:
            return None

        return Snapshot(record[0], record[1])
    
    getLastReq = "select id, date from snapshot order by date desc limit 1"
    def getLast(self):
        self.cursor.execute(SnapshotRepo.getLastReq, (id,))
        record = self.cursor.fetchone()

        if record is None:
            return None

        return Snapshot(record[0], record[1])

    getListReq = "select id, date from snapshot"
    def getList(self):
        self.cursor.execute(SnapshotRepo.getListReq)
        res = []
        for record in self.cursor:
            res.append(Snapshot(record[0], record[1]))
        return res

    addReq = "insert into snapshot(date) values (%s)"
    def add(self, snapshot):
        self.cursor.execute(SnapshotRepo.addReq, (snapshot.date,))
        self.cursor.execute('SELECT LASTVAL()')
        snapshot.id = self.cursor.fetchone()[0]
        return snapshot