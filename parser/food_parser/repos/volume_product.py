import food_parser.core as core

class VolumeProductRepo:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = conn.cursor()
    
    def __getByQuery(self, query, params = None):
        self.cursor.execute(query, params)
        res = []
        for record in self.cursor:
            prod = core.createVolumeProduct(record[2], record[4], record[5], record[6], record[8], record[0], record[7], record[1])
            if(prod is None):
                print(record)
            res.append(prod)
        return res

    getByIdReq = "select id, snapshot_id, type, subtype, name, volume, price, shop_id, url from volume_product where id = %s"
    def getById(self, id):
        self.cursor.execute(VolumeProductRepo.getByIdReq, (id,))
        record = self.cursor.fetchone()

        if record is None:
            return None

        return core.createWeightProduct(record[2], record[4], record[5], record[6], record[8], record[0], record[7], record[1])

    getListReq = "select id, snapshot_id, type, subtype, name, volume, price, shop_id, url from volume_product"
    def getList(self):
        return self.__getByQuery(VolumeProductRepo.getListReq)
    
    def getByQuery(self, params = dict()):
        req = VolumeProductRepo.getListReq

        condClauses = []

        if "snapshot" in params:
            condClauses.append(" snapshot_id = %(snapshot)s ")
        if "shop" in params:
            condClauses.append(" shop_id = %(shop)s ")
        if "type" in params:
            condClauses.append(" type = %(type)s ")
        if "name" in params:
            condClauses.append(" lower(name) like %(name)s ")
            params["name"] = "%" + params["name"].lower() + "%"

        # TODO add another filters like min/max price, min/max price per liter,
        # min/max volume

        if len(condClauses):
            req +=" where " + " and ".join(condClauses)
        
        return self.__getByQuery(req, params)
    

    addReq = "insert into volume_product (snapshot_id, type, subtype, name, volume, price, shop_id, url) VALUES (%(snapId)s, %(type)s, %(subtype)s, %(name)s, %(volume)s, %(price)s, %(shopId)s, %(url)s)"
    def add(self, product):
        self.cursor.execute(VolumeProductRepo.addReq, core.productToDict(product))
        self.cursor.execute('SELECT LASTVAL()')
        product.id = self.cursor.fetchone()[0]
        return product