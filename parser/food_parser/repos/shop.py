# from parser.core import Shop
from food_parser.core import Shop, Shops

class ShopRepo:

    def __init__(self, conn):
        self.conn = conn
        self.cursor = conn.cursor()

    getByIdReq = "select id, name from shop where id = %s"
    def getById(self, id):
        self.cursor.execute(ShopRepo.getByIdReq, (id,))
        rawShop = self.cursor.fetchone()

        if rawShop is None:
            return None

        return Shop(rawShop[0], rawShop[1])

    getByNameReq = "select id, name from shop where lower(name) like %s"
    def getByName(self, name):
        self.cursor.execute(ShopRepo.getByNameReq, ("%"+name.lower()+"%",))
        res = []
        for record in self.cursor:
            res.append(Shop(record[0], record[1]))
        return res
    
    def getByEnum(self, shop):
        shopToStr = {
            Shops.Pyatorochka: "пятёрочка",
            Shops.Perekrestok: "перекрёсток",
            Shops.Dixy: "дикси",
            Shops.Magnit: "магнит",
            Shops.Da: "да",
            Shops.Fixprice: "fix price",
            Shops.Selgros: "зельгрос",
            Shops.Globus: "глобус",
            Shops.Auchan: "ашан",
        }
        
        if shop not in shopToStr:
            return None
        
        shops = self.getByName(shopToStr[shop])

        if len(shops) == 0:
            return None
        
        return shops[0]


    getListReq = "select id, name from shop"
    def getList(self):
        self.cursor.execute(ShopRepo.getListReq)
        res = []
        for record in self.cursor:
            res.append(Shop(record[0], record[1]))
        return res


class CachingShopRepo(ShopRepo):
    def __init__(self, conn):
        super().__init__(conn)
    
    def getByEnum(self, shop):
        if shop in CachingShopRepo.enumShopMap:
            return CachingShopRepo.enumShopMap[shop]
        
        s = super().getByEnum(shop)

        if s is not None:
            CachingShopRepo.enumShopMap[shop] = s
        
        return s

CachingShopRepo.enumShopMap = {}

def getShopRepo(conn):
    return CachingShopRepo(conn)