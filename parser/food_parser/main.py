import psycopg2
import os

from food_parser.core import *
from food_parser.logger import *

#import repositories
from food_parser.repos.shop import getShopRepo
from food_parser.repos.snapshot import SnapshotRepo
from food_parser.repos.volume_product import VolumeProductRepo
from food_parser.repos.weight_product import WeightProductRepo

# import parsers
import food_parser.pyatorochka as pyatorochka
import food_parser.perekrestok as perekrestok
import food_parser.dixy as dixy
import food_parser.magnit as magnit
import food_parser.da as da
import food_parser.fixprice as fixprice
# import food_parser.selgros as selgros
import food_parser.globus as globus
import food_parser.auchan as auchan

from food_parser.dump_db import dumpDb


import requests_cache
# setup caching of requests
requests_cache.install_cache(
    'req_cache', backend='sqlite', expire_after=60*20)


# Array of fetchers
fetchers = {
    ProductType.Meat: {
        Shops.Pyatorochka: [pyatorochka.SausageFetcher],
        Shops.Perekrestok: [perekrestok.SausageFetcher, perekrestok.MeatFetcher, perekrestok.BirdFetcher],
        Shops.Dixy: [dixy.SausageFetcher, dixy.SausageFetcher2, dixy.MeatFetcher, dixy.BirdFetcher],
        Shops.Magnit: [magnit.SausageFetcher],
        Shops.Da: [da.SausageFetcher, da.MeatFetcher, da.BirdFetcher],
        # Shops.Selgros: [SausageSelgrosFetcher, MeatSelgrosFetcher, BirdSelgrosFetcher],
        Shops.Globus: [globus.SausageFetcher, globus.MeatFetcher, globus.BirdFetcher],
        Shops.Auchan: [auchan.SausageFetcher, auchan.MeatFetcher, auchan.BirdFetcher],
    },
    ProductType.Cheese: {
        Shops.Pyatorochka: [pyatorochka.CheeseFetcher],
        Shops.Perekrestok: [perekrestok.CheeseFetcher],
        Shops.Dixy: [dixy.CheeseFetcher, dixy.CheeseFetcher2],
        Shops.Magnit: [magnit.CheeseFetcher],
        Shops.Da: [da.CheeseFetcher],
        # Shops.Selgros: [selgros.CheeseSelgrosFetcher],
        Shops.Globus: [globus.CheeseFetcher],
        Shops.Auchan: [auchan.CheeseFetcher]
    },
    ProductType.Fish: {
        Shops.Perekrestok: [perekrestok.FishFetcher],
        Shops.Dixy: [dixy.FishFetcher],
        # Shops.Selgros: [selgros.FishSelgrosFetcher],
        Shops.Globus: [globus.FishFetcher],
        Shops.Auchan: [auchan.FishFetcher]
    },
    ProductType.Water: {
        Shops.Pyatorochka: [pyatorochka.WaterFetcher],
        Shops.Perekrestok: [perekrestok.WaterFetcher],
        Shops.Dixy: [dixy.WaterFetcher, dixy.WaterFetcher2],
        Shops.Magnit: [magnit.WaterFetcher],
        Shops.Da: [da.WaterFetcher],
        Shops.Fixprice: [fixprice.WaterFixpriceFetcher],
        # Shops.Selgros: [selgros.WaterSelgrosFetcher],
        Shops.Globus: [globus.WaterFetcher],
        Shops.Auchan: [auchan.WaterFetcher]
    },
    ProductType.Vegetable: {
        Shops.Pyatorochka: [pyatorochka.VegetableFetcher],
        Shops.Perekrestok: [perekrestok.VegetableFetcher],
        Shops.Dixy: [dixy.VegetableFetcher, dixy.VegetableFetcher2],
        Shops.Magnit: [magnit.VegetableFetcher],
        Shops.Da: [da.VegetableFetcher],
        # Shops.Selgros: [selgros.VegetableSelgrosFetcher],
        Shops.Globus: [globus.VegetableFetcher],
        Shops.Auchan: [auchan.VegetableFetcher]
    },
    ProductType.Fruit: {
        Shops.Pyatorochka: [pyatorochka.FruitFetcher],
        Shops.Perekrestok: [perekrestok.FruitFetcher],
        Shops.Dixy: [dixy.FruitFetcher, dixy.FruitFetcher2],
        Shops.Magnit: [magnit.FruitFetcher],
        Shops.Da: [da.FruitFetcher],
        # Shops.Selgros: [selgros.FruitSelgrosFetcher],
        Shops.Globus: [globus.FruitFetcher],
        Shops.Auchan: [auchan.FruitFetcher]
    },
    ProductType.MilkyDrink: {
        Shops.Pyatorochka: [pyatorochka.MilkyDrinkFetcher],
        Shops.Perekrestok: [perekrestok.MilkyDrinkFetcher],
        Shops.Dixy: [dixy.MilkyDrinkFetcher, dixy.MilkyDrinkFetcher2],
        Shops.Magnit: [magnit.MilkyDrinkFetcher],
        Shops.Da: [da.MilkyDrinkFetcher],
        # Shops.Selgros: [selgros.MilkyDrinkSelgrosFetcher],
        Shops.Globus: [globus.MilkyDrinkFetcher],
        Shops.Auchan: [auchan.MilkyDrinkFetcher],
    },
    ProductType.Nut: {
        Shops.Pyatorochka: [pyatorochka.NutFetcher],
        Shops.Perekrestok: [perekrestok.NutFetcher],
        Shops.Dixy: [dixy.NutFetcher, dixy.NutFetcher2],
        Shops.Magnit: [magnit.NutFetcher],
        Shops.Da: [da.NutFetcher],
        Shops.Fixprice: [fixprice.NutFixpriceFetcher],
        # Shops.Selgros: [selgros.NutSelgrosFetcher],
        Shops.Globus: [globus.NutFetcher],
        Shops.Auchan: [auchan.NutFetcher]
    }
}

if __name__ == "__main__":
    logger.log(LogLevel.INFO, "starts prices parsing")

    conn = psycopg2.connect(
        dbname=os.getenv('DB_DATABASE', 'productdb'),
        user=os.getenv('DB_USER', 'postgres'),
        password=os.getenv('DB_PASSWORD', 'postgres'),
        host=os.getenv('DB_HOST', '127.0.0.1'))

    snapRepo = SnapshotRepo(conn)

    # if we have already fetched data today than do nothing
    lastSnap = snapRepo.getLast()
    currentDate = datetime.datetime.now().date()
    dateDiff = currentDate - lastSnap.date.date()
    if dateDiff.days == 0:
        logger.log(LogLevel.INFO, "data is already up-to-date")
        exit()
    
    shopRepo = getShopRepo(conn)

    # Fetch products
    data = {}
    for ptype, pfetchers in fetchers.items():
        products = []
        for shopId, shopFetchers in pfetchers.items():

            shop = shopRepo.getByEnum(shopId)
            # If the shop is not in the database than do nothing
            if shop is None:
                continue

            shopProds = []
            for fetcher in shopFetchers:
                fetchProds = fetcher().fetch()
                shopProds += fetchProds if fetchProds else []
            for prod in shopProds:
                prod.shopId = shop.id
            products += shopProds
        data[ptype] = products

    logger.log(LogLevel.INFO, "finished prices parsing")
    logger.log(LogLevel.INFO, "starts db writing")

    # Put the snapshot to the database
    snap = snapRepo.add(Snapshot(None, currentDate))

    wpRepo = WeightProductRepo(conn)
    vpRepo = VolumeProductRepo(conn)

    for ptype, prods in data.items():
        for prod in prods:
            prod.snapId = snap.id
            if isinstance(prod, WeightProduct):
                wpRepo.add(prod)
            if isinstance(prod, VolumeProduct):
                vpRepo.add(prod)

    conn.commit()
    logger.log(LogLevel.INFO, "finished db writing")

    #dump database
    dumpDb()
