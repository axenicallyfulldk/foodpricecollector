import enum
import time
import re
import numpy as np


class Shops(enum.IntEnum):
    Pyatorochka = 1
    Perekrestok = 2
    Dixy = 3
    Magnit = 4
    Da = 5
    Fixprice = 6
    Selgros = 7
    Globus = 8
    Auchan = 9


class Shop:
    def __init__(self, id, name):
        self.id = id
        self.name = name

class Snapshot:
    def __init__(self, id, date):
        self.id = id
        self.date = date


class ProductType(enum.IntEnum):
    Meat = 1
    Fish = 2
    Cheese = 3
    Water = 4
    Vegetable = 5
    Fruit = 6
    MilkyDrink = 7
    Nut = 8


class WeightProduct:
    def __init__(self, name, weight, price, url = None, id = None, shopId = None, snapId = None):
        self.id = id
        self.shopId = shopId
        self.snapId = snapId
        self.name = name
        self.weight = weight
        self.price = price
        self.url = url

    def __str__(self):
        return "{} | {} г | {} руб".format(self.name, self.weight, self.price/100)


class Cheese(WeightProduct):
    productType = ProductType.Cheese

    def __init__(self, name, weight, price, url = None, id = None, shopId = None, snapId = None):
        super().__init__(name, weight, price, url, id, shopId, snapId)


class Fish(WeightProduct):
    productType = ProductType.Fish

    def __init__(self, name, weight, price, url = None, id = None, shopId = None, snapId = None):
        super().__init__(name, weight, price, url, id, shopId, snapId)


class Sausage(WeightProduct):
    productType = ProductType.Meat

    def __init__(self, name, weight, price, url = None, id = None, shopId = None, snapId = None):
        super().__init__(name, weight, price, url, id, shopId, snapId)


class Meat(WeightProduct):
    productType = ProductType.Meat

    def __init__(self, name, weight, price, url = None, id = None, shopId = None, snapId = None):
        super().__init__(name, weight, price, url, id, shopId, snapId)


class Bird(WeightProduct):
    productType = ProductType.Meat

    def __init__(self, name, weight, price, url = None, id = None, shopId = None, snapId = None):
        super().__init__(name, weight, price, url, id, shopId, snapId)


class Vegetable(WeightProduct):
    productType = ProductType.Vegetable

    def __init__(self, name, weight, price, url = None, id = None, shopId = None, snapId = None):
        super().__init__(name, weight, price, url, id, shopId, snapId)


class Fruit(WeightProduct):
    productType = ProductType.Fruit

    def __init__(self, name, weight, price, url = None, id = None, shopId = None, snapId = None):
        super().__init__(name, weight, price, url, id, shopId, snapId)


class Nut(WeightProduct):
    productType = ProductType.Nut

    def __init__(self, name, weight, price, url = None, id = None, shopId = None, snapId = None):
        super().__init__(name, weight, price, url, id, shopId, snapId)


class VolumeProduct:
    def __init__(self, name, volume, price, url = None, id = None, shopId = None, snapId = None):
        self.id = id
        self.shopId = shopId
        self.snapId = snapId
        self.name = name
        self.volume = volume
        self.price = price
        self.url = url

    def __str__(self):
        return "{} | {} л | {} руб".format(self.name, self.volume/1000, self.price/100)


class Water(VolumeProduct):
    productType = ProductType.Water

    def __init__(self, name, volume, price, url = None, id = None, shopId = None, snapId = None):
        super().__init__(name, volume, price, url, id, shopId, snapId)


class MilkyDrinkVolume(VolumeProduct):
    productType = ProductType.MilkyDrink

    def __init__(self, name, volume, price,  url = None, id = None, shopId = None, snapId = None):
        super().__init__(name, volume, price, url, id, shopId, snapId)


class MilkyDrinkWeight(WeightProduct):
    productType = ProductType.MilkyDrink

    def __init__(self, name, weight, price,  url = None, id = None, shopId = None, snapId = None):
        super().__init__(name, weight, price, url, id, shopId, snapId)

def productToDict(product):
    prodDict = product.__dict__
    if hasattr(product, "productType"):
        prodDict["type"] = product.productType
    
    if "subtype" not in prodDict:
        prodDict["subtype"] = None
    
    if "url" not in prodDict:
        prodDict["url"] = None

    return prodDict

def createWeightProduct(type, name, weight, price, url = None, id = None, shopId = None, snapId = None):
    if type == ProductType.Meat:
        return Meat(name, weight, price, id, shopId, snapId)
    elif type == ProductType.Fish:
        return Fish(name, weight, price, id, shopId, snapId)
    elif type == ProductType.Cheese:
        return Cheese(name, weight, price, id, shopId, snapId)
    elif type == ProductType.Vegetable:
        return Vegetable(name, weight, price, id, shopId, snapId)
    elif type == ProductType.Fruit:
        return Fruit(name, weight, price, id, shopId, snapId)
    elif type == ProductType.Nut:
        return Nut(name, weight, price, id, shopId, snapId)
    elif type == ProductType.MilkyDrink:
        return MilkyDrinkWeight(name, weight, price, id, shopId, snapId)
    else:
        return None

def createVolumeProduct(type, name, volume, price, url = None, id = None, shopId = None, snapId = None):
    if type == ProductType.Water:
        return Water(name, volume, price, id, shopId, snapId)
    elif type == ProductType.MilkyDrink:
        return MilkyDrinkVolume(name, volume, price, id, shopId, snapId)
    else:
        return None

def humanLag(customMu = None):
    mu, sigma = 7.34, 1.32
    # mu, sigma = 3.34, 1.32
    # mu, sigma = 1.34, 1.32

    if customMu is not None:
        mu = customMu

    s = np.random.normal(mu, sigma, 1)[0]
    while s < 0:
        s = np.random.normal(mu, sigma, 1)[0]
    
    time.sleep(s)


# Array of regular expressions, indices and multipliers for extracting product
# name and weight in gramms
# (regexpr, (index of name group, index of weight group), weight multiplier)
name_weight_regexp = [
    (r'^([А-Яа-яЁё\w\s\d\-\+%№&®"`«»,\.\/\\;!\(\)]+)\*\s*цена указана за (100)\s?[гГ]', (1, 2), 1),
    (r'^([А-Яа-яЁё\w\s\d\-\+%№&®"`«»,\.\/\\;!\(\)]+)[\s\.,]+(\d+)\s?[гГ]', (1, 2), 1),
    (r'^([А-Яа-яЁё\w\s\d\-\+%№&®"`«»,\.\/\\;!\(\)]+?)[\s\.,]+([0-9]+[\.,]?[0-9]*)\s?(кг|КГ)', (1, 2), 1000),
]

def extractNameAndWeight(productName):
    for rnw in name_weight_regexp:
        regRes = re.search(rnw[0], productName)
        if regRes:
            name = regRes.group(rnw[1][0]).strip(", ")
            size = int(
                float(regRes.group(rnw[1][1]).replace(",", "."))*rnw[2])
            return name, size
    
    return None, None

# Array of regular expressions, indices and multipliers for extracting product
# name and weight in millilitres
# (regexpr, (index of name group, index of volume group), volume multiplier)
name_volume_regexp = [
    (r'^([А-Яа-яЁё\w\s\-\+\d%,\.\/;№&®"`«»\(\)]+?)[\s\.,]+([0-9]+[\.,]?[0-9]*)\s?[лЛ]', (1, 2), 1000),
    (r'^([А-Яа-яЁё\w\s\-\+\d%,\.\/;№&®"`«»\(\)]+?)[\s\.,]+([0-9]+[\.,]?[0-9]*)\s?(мл|МЛ)', (1, 2), 1)
]

def extractNameAndVolume(productName):
    for rnw in name_volume_regexp:
        regRes = re.search(rnw[0], productName)
        if regRes:
            name = regRes.group(rnw[1][0]).strip(", ")
            size = int(
                float(regRes.group(rnw[1][1]).replace(",", "."))*rnw[2])
            return name, size
    
    return None, None
