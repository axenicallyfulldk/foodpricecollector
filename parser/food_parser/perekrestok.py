import requests
import bs4
from urllib.parse import urljoin

from food_parser.core import *
from food_parser.logger import *
from food_parser.prodtype_criteria import *

class GenericProductFetcher:

    MAX_PAGE = 30

    def __init__(self, urlTemplate, productCriterion, weightProductClass, volumeProductClass):
        self.urlTemplate = urlTemplate
        self.productCriterion = productCriterion
        self.weightProductClass = weightProductClass
        self.volumeProductClass = volumeProductClass

    def fetch(self):
        res = []
        for pi in range(1, self.MAX_PAGE):
            pageUrl = self.urlTemplate.format(pi)
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
                'Content-type': 'application/json'}
            resp = requests.get(pageUrl, headers=headers)

            # When page with products doesn't exist the server returns html
            # not a json
            if resp.headers["content-type"].find("json") == -1:
                return res

            humanLag()

            if not resp:
                logger.log(LogLevel.ERROR, "Failed to fetch info by url {}".format(
                    pageUrl))
                return res

            pageRes = self.__parseResponse(resp)
            if pageRes is None:
                break

            res += pageRes

        return res

    def __parseResponse(self, resp):
        try:
            data = resp.json()
        except ValueError:
            logger.log(LogLevel.ERROR, "Perekrestok Failed to parse response {} fetched by url {}".format(
                resp.text[:10], resp.request.url))
            return None

        tree = bs4.BeautifulSoup(data["html"], 'lxml')

        items = tree.findAll("li", {"class": "xf-catalog__item"})

        res = []
        for item in items:
            hasMatch = False

            nameEl = item.find(
                "a", {"class": "xf-product-title__link"})
            if not nameEl:
                logger.log(LogLevel.WARNING,
                           "Perekrestok Failed to find name element {}".format(item))
                continue
            nameRaw = nameEl.text.strip("\n ")

            if self.productCriterion and not self.productCriterion(nameRaw):
                continue

            priceEl = item.find("div", {"data-cost": True, "data-type": True})
            if not priceEl:
                continue
            price = int(float(priceEl.attrs["data-cost"])*100)

            hasMatch = False
            prodClass = self.weightProductClass
            if priceEl.attrs["data-type"].lower() == "100г":
                hasMatch = True
                name = nameRaw
                size = 100
            elif priceEl.attrs["data-type"].lower() == "кг":
                hasMatch = True
                name = nameRaw
                size = 1000
            else:
                name, size = extractNameAndWeight(nameRaw)
                hasMatch = name is not None and size is not None

            if not hasMatch and self.volumeProductClass:
                prodClass = self.volumeProductClass
                name, size = extractNameAndVolume(nameRaw)
                hasMatch = name is not None and size is not None

            if not hasMatch:
                logger.log(
                    LogLevel.WARNING, 'Perekrestok Failed to parse product name "{}"'.format(nameRaw))
                continue

            product = prodClass(name, size, price)
            product.url = urljoin(resp.url, item.find(
                "a", {"class": "xf-product-title__link"}).attrs["href"])
            res.append(product)

        return res


class SausageFetcher():
    def __init__(self):
        # Sausage
        self.f1 = GenericProductFetcher(
            r'https://www.vprok.ru/catalog/1387/delikatesy-i-kolbasnye-izdeliya?attr[rate][]=0&page={}&sort=price_asc&ajax=true', None, Sausage, None)
        
        
        # Weenie        
        self.f2 = GenericProductFetcher(
            r'https://www.vprok.ru/catalog/1349/sosiski-sardelki-shpikachki?attr[rate][]=0&page={}&sort=price_asc&ajax=true', None, Sausage, None)

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()


class MeatFetcher():
    def __init__(self):
        # Beef
        self.f1 = GenericProductFetcher(
            r'https://www.vprok.ru/catalog/1385/govyadina?attr[rate][]=0&page={}&sort=price_asc&ajax=true', None, Meat, None)
        # Pig
        self.f2 = GenericProductFetcher(
            r'https://www.vprok.ru/catalog/1394/svinina?attr[rate][]=0&page={}&sort=price_asc&ajax=true', None, Meat, None)

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()


class BirdFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://www.vprok.ru/catalog/1392/ptitsa?attr[rate][]=0&page={}&sort=price_asc&ajax=true', None, Bird, None)
            


class CheeseFetcher(GenericProductFetcher):
    def __init__(self):
        super(CheeseFetcher, self).__init__(
            r'https://www.vprok.ru/catalog/1381/syry?attr[1613][]=polutverdyy&attr[1613][]=tverdyy&attr[rate][]=0&page={}&sort=price_asc&ajax=true', None, Cheese, None)
        


class FishFetcher(GenericProductFetcher):
    def __init__(self):
        super(FishFetcher, self).__init__(
            r'https://www.vprok.ru/catalog/1411/ryba-solenaya?attr[rate][]=0&page={}&sort=price_asc&ajax=true', None, Fish, None)


class VegetableFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://www.vprok.ru/catalog/1399/ovoschi?attr[rate][]=0&page={}&sort=price_asc&ajax=true', isVegetable, Vegetable, None)


class FruitFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://www.vprok.ru/catalog/1401/frukty?attr[rate][]=0&page={}&sort=price_asc&ajax=true', isFruit, Fruit, None)


class NutFetcher(GenericProductFetcher):
    def __init__(self):
        self.f1 = GenericProductFetcher(
            r'https://www.vprok.ru/catalog/1343/orehi?attr[rate][]=0&page={}&sort=price_asc&ajax=true', isNut, Nut, None)
        
        self.f2 = GenericProductFetcher(
            r'https://www.vprok.ru/catalog/1366/semechki-suhofrukty?attr[rate][]=0&page={}&sort=price_asc&ajax=true', isNut, Nut, None)
        

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()


class WaterFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://www.vprok.ru/catalog/1415/gazirovannye-napitki?attr[rate][]=0&page=2&sort=price_asc&ajax=true', None, None, Water)
        


class MilkyDrinkFetcher():
    def __init__(self):
        self.firstFetcher = GenericProductFetcher(
            r'https://www.vprok.ru/catalog/1375/kislomolochnye-produkty?attr[rate][]=0&page={}&sort=price_asc&ajax=true', isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)

        self.secondFetcher = GenericProductFetcher(
            r'https://www.vprok.ru/catalog/1377/moloko?attr[rate][]=0&page={}&sort=price_asc&ajax=true', isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)

        self.thirdFetcher = GenericProductFetcher(
            r'https://www.vprok.ru/catalog/2660/kefir?attr[rate][]=0&page={}&sort=price_asc&ajax=true', isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)

    def fetch(self):
        return self.firstFetcher.fetch() + self.secondFetcher.fetch() + self.thirdFetcher.fetch()


if __name__ == '__main__':
    data = SausageFetcher().fetch()
    ij = 3