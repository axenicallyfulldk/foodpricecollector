import requests
import bs4
from urllib.parse import urljoin

from food_parser.core import *
from food_parser.logger import *
from food_parser.prodtype_criteria import *


class GenericProductFixpriceFetcher:

    MAX_PAGE = 20

    def __init__(self, urlTemplate, productCriterion, weightProductClass, volumeProductClass):
        self.urlTemplate = urlTemplate
        self.productCriterion = productCriterion
        self.weightProductClass = weightProductClass
        self.volumeProductClass = volumeProductClass

    def fetch(self):
        res = []
        self.handledProducts = {}
        for pi in range(1, self.MAX_PAGE):
            pageUrl = self.urlTemplate.format(pi)
            resp = requests.get(pageUrl)

            if not resp:
                logger.log(LogLevel.ERROR, "Failed to fetch info by url {}".format(
                    pageUrl))
                return res

            pageRes = self.__parseResponse(resp)

            if pageRes is None:
                return res

            if res is None:
                res = pageRes
            else:
                res += pageRes

        return res

    def __parseResponse(self, resp):
        tree = bs4.BeautifulSoup(resp.text, 'lxml')

        items = tree.findAll("div", {"class": "main-list__card-item"})

        res = []
        for item in items:
            prodId = item.find("a", {"data-id": True})["data-id"]

            if prodId in self.handledProducts:
                if res:
                    return res
                else:
                    return None
            self.handledProducts[prodId] = True

            nameRaw = item.find(
                "span", {"itemprop": "name"}).text.strip("\r\n ")

            if not self.productCriterion(nameRaw):
                continue

            priceRaw = item.find("span", {"itemprop": "price"})["data-price"]

            productClass = self.weightProductClass
            name, size = extractNameAndWeight(nameRaw)
            hasMatch = name is not None and size is not None

            if not hasMatch and self.volumeProductClass:
                productClass = self.volumeProductClass
                name, size = extractNameAndVolume(nameRaw)
                hasMatch = name is not None and size is not None

            if not hasMatch:
                logger.log(
                    LogLevel.WARNING, 'Fixprice Failed to parse product name "{}"'.format(nameRaw))
                continue

            price = int(float(priceRaw)*100)

            product = productClass(name, size, price)
            product.url = urljoin(resp.url, item.find(
                "a", {"data-id": True}).attrs["href"])

            res.append(product)

        return res


class NutFixpriceFetcher():
    def __init__(self):
        self.f1 = GenericProductFixpriceFetcher(
            r'https://fix-price.ru/catalog/produkty-i-napitki/zakuski/?sortBy=PROPERTY_SORT_PRICE&sortOrder=asc&PAGEN_2={}', isNut, Nut, None)

        self.f2 = GenericProductFixpriceFetcher(
            r'https://fix-price.ru/catalog/produkty-i-napitki/bakaleya/?sortBy=PROPERTY_SORT_PRICE&sortOrder=asc&PAGEN_2={}', isNut, Nut, None)

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()


class WaterFixpriceFetcher(GenericProductFixpriceFetcher):
    def __init__(self):
        super().__init__(
            r'https://fix-price.ru/catalog/produkty-i-napitki/napitki-soki-vody/?sortBy=PROPERTY_SORT_PRICE&sortOrder=asc&PAGEN_2={}', lambda name: name.upper().find("НАПИТОК") > -1, None, Water)


if __name__ == '__main__':
    data = NutFixpriceFetcher().fetch()