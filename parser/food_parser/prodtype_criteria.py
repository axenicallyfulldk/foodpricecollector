import re

vegetables = ["топинамбур", "батат", "картофель", "морковь", "свекла", "свёкла", "репа", "брюква", "редька", "редис", "петрушка", "пастернак", "сельдерей", "хрен", "дайкон", "капуста", "брокколи", "салат", "укроп", "эстрагон",
              "чабер", "базилик", "майоран", "лук", "чеснок", "томат", "перец", "баклажан", "тыква", "кабачок", "цуккини", "огурец", "патиссон", "дыня", "арбуз", "горох", "боб", "чечевица", "кукуруза", "артишок", "спаржа", "ревень"]
              
vegetableKeys = ["топинамбур", "батат", "картофель", "морковь", "свекла", "свёкла", "репа", "брюква", "редька", "редис", "петрушка", "пастернак", "сельдерей", "хрен", "дайкон", "капуста", "брокколи", "салат", "укроп", "эстрагон",
                 "чабер", "базилик", "майоран", "лук", "чеснок", "томат", "томаты", "перец", "баклажан", "тыква", "тыквы", "кабачок", "кабачки", "цуккин", "огуркц", "огурцы", "патисс", "дыня", "дыни", "арбуз", "горох", "боб", "чечевиц", "кукуруза", "артишок", "спаржа", "ревень"]

fruits = ["яблоко", "абрикос", "авокадо", "манго", "папайя",
          "персик", "нектарин", "груша", "слива", "ананас", "банан", "кокос", "маракуйя"]

fruitKeys = ["фрукт", "яблоко", "яблоки", "абрикос", "абрикосы", "авокадо", "манго", "папайя",
             "персик", "персики", "нектарин", "нектарины", "груша", "груши", "слива", "сливы", "ананас", "ананасы", "банан", "бананы", "кокос", "кокосы", "маракуйя"]

nuts = ["грецкий", "фундук", "арахис", "миндаль", "кешью", "макадамия", "кедровый",
        "лесной", "лещина", "медвежий", "кокос", "бразильский", "пекан", "фисташки", "каштан"]

nutKeys = ["грецкий", "грецкого ореха", "фундук", "фундука", "арахис", "миндаль", "кешью", "макадамия", "орех кедровый", "кедровый орех", "кедровые",
           "лесной", "лещина", "медвежий", "кокос", "кокосы", "бразильский", "пекан", "фисташка", "фисташки", "орехи фисташковые", "фисташковые орехи", "каштан", "каштаны"]


def isVegetable(name):
    lowName = name.lower()
    for vegetKey in vegetableKeys:
        if re.search(r"([^а-я]|^){}([^а-я]|$)".format(vegetKey), lowName):
            return True
    return False


def isFruit(name):
    lowName = name.lower()
    for fruitKey in fruitKeys:
        if re.search(r"([^а-я]|^){}([^а-я]|$)".format(fruitKey), lowName):
            return True
    return False


def isNut(name):
    lowName = name.lower()
    for nutKey in nutKeys:
        if re.search(r"([^а-я]|^){}([^а-я]|$)".format(nutKey), lowName):
            return True
    return lowName.find("семечки") > -1 or lowName.find("семена") > -1


def isCheese(name):
    upperName = name.upper()
    return re.search(r"(^|\s)СЫР(\s|$)", upperName) is not None


milkyDrinks = ["молоко", "айран", "арака", "арса", "ацидофилин", "варенец", "гуслянка", "катык", "кефир",
               "кумыс", "ласси", "мацони", "простокваша", "ряженка", "снежок", "тан", "тарак", "турах", "чалоп", "шубат"]
milkyDrinkKeys = ["молоко", "айран", "арака", "арса", "ацидофилин", "варенец", "гуслянка", "катык", "кефир",
                  "кумыс", "ласси", "мацони", "простокваша", "ряженка", "снежок", "тан", "тарак", "турах", "чалоп", "шубат"]


def isMilkyDrink(name):
    lowName = name.lower()
    for key in milkyDrinkKeys:
        if re.search(r"([^а-я]|^){}([^а-я]|$)".format(key), lowName):
            return True
    return False

def isSausage(name):
    upperName = name.upper()
    return upperName.find("КОЛБАСА") >= 0 or upperName.find("ВЕТЧИНА") >= 0 or upperName.find("СОСИС") >= 0


def isBird(name):
    upperName = name.upper()
    if upperName.find("ЦЫПЛ") > -1:
        return True

    if upperName.find("КУРИ") > -1:
        return True

    if upperName.find("ИНДЕЙ") > -1:
        return True