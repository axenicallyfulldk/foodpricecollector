import requests
import re
import bs4
from urllib.parse import urljoin

from food_parser.core import *
from food_parser.logger import *
from food_parser.prodtype_criteria import *


class GenericProductFetcher:

    MAX_PAGE = 10

    def __init__(self, urlTemplate, productCriterion, weightProductClass, volumeProductClass):
        self.urlTemplate = urlTemplate
        self.productCriterion = productCriterion
        self.weightProductClass = weightProductClass
        self.volumeProductClass = volumeProductClass

    def fetch(self):
        res = []
        for pi in range(1, self.MAX_PAGE):
            pageUrl = self.urlTemplate.format(pi)
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'}
            # resp = requests.get(pageUrl, headers=headers)
            try:
                resp = requests.get(pageUrl, headers=headers, cookies={
                                    "globus_hyper_id": "27495", "globus_hyper_name": "%D0%9A%D0%BE%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D0%B8"})
            except Exception:
                continue
            # if resp.history:
            #     return res

            if not hasattr(resp, "from_cache") or not resp.from_cache:
                humanLag()

            if not resp:
                logger.log(LogLevel.ERROR, "Failed to fetch info by url {}".format(
                    pageUrl))
                return res

            pageRes = self.__parseResponse(resp)
            if pageRes is None:
                break

            res += pageRes

        # if res is None:
        #     logger.log(LogLevel.WARNING, "There was not find any product by template {}".format(
        #         self.urlTemplate))
        #     res = []

        return res

    def __parseResponse(self, resp):
        try:
            data = resp.text
        except ValueError:
            logger.log(LogLevel.ERROR, "Failed to parse response {} fetched by url {}".format(
                resp.text[:10], resp.request.url))
            return None

        tree = bs4.BeautifulSoup(data, 'lxml')

        items = set(tree.findAll("a", {"class": "pim-list__item"})) - \
            set(tree.findAll("a", {"class": "pim-list__item--hidden"}))

        if not items:
            return None

        res = []
        for item in items:
            hasMatch = False

            nameEl = item.find(
                "div", {"data-full-text": True})
            if not nameEl:
                logger.log(LogLevel.WARNING,
                           "Globus Failed to find name element {}".format(item))
                continue
            nameRaw = nameEl.text.strip("\n ")

            if self.productCriterion and not self.productCriterion(nameRaw):
                continue

            priceEl = item.find(
                "div", {"class": "pim-list__item-price-actual"})
            if not priceEl:
                continue
            priceStr = re.sub(r'(\d)\s(\d)', r'\1.\2', priceEl.text)
            price = int(float(priceStr)*100)

            hasMatch = False
            prodClass = self.weightProductClass

            if self.weightProductClass:
                for rnw in name_weight_regexp:
                    regRes = re.search(rnw[0], nameRaw)
                    if regRes:
                        hasMatch = True
                        name = regRes.group(rnw[1][0]).strip(", ")
                        size = int(
                            float(regRes.group(rnw[1][1]).replace(",", "."))*rnw[2])
                        break

            if not hasMatch and self.volumeProductClass:
                prodClass = self.volumeProductClass
                for rnw in name_volume_regexp:
                    regRes = re.search(rnw[0], nameRaw)
                    if regRes:
                        hasMatch = True
                        name = regRes.group(rnw[1][0]).strip(", ")
                        size = int(
                            float(regRes.group(rnw[1][1]).replace(",", "."))*rnw[2])
                        break

            if not hasMatch:
                logger.log(
                    LogLevel.WARNING, 'Globus Failed to parse product name "{}"'.format(nameRaw))
                continue

            product = prodClass(name, size, price)
            product.url = urljoin(resp.url, item.attrs["href"])
            res.append(product)

        return res


class SausageFetcher():
    def __init__(self):
        self.f1 = GenericProductFetcher(
            r'https://www.globus.ru/catalog/myaso-ryba-kulinariya/myasnaya-gastronomiya/kolbasa/?count=36&sort=price_asc&page={}', None, Sausage, None)
        self.f2 = GenericProductFetcher(
            r'https://www.globus.ru/catalog/myaso-ryba-kulinariya/myasnaya-gastronomiya/vetchina/?count=36&sort=price_asc&page={}', None, Sausage, None)

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()


class MeatFetcher():
    def __init__(self):
        # Pig
        self.f1 = GenericProductFetcher(
            r'https://www.globus.ru/catalog/myaso-ryba-kulinariya/myaso/svinina/?count=36&sort=price_asc&page={}', None, Meat, None)
        # Beef
        self.f2 = GenericProductFetcher(
            r'https://www.globus.ru/catalog/myaso-ryba-kulinariya/myaso/govyadina/?count=36&sort=price_asc&page={}', None, Meat, None)

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()


class BirdFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://www.globus.ru/catalog/myaso-ryba-kulinariya/ptitsa/okhlazhdyennaya-ptitsa/?count=36&sort=price_asc&page={}', None, Bird, None)


class CheeseFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://www.globus.ru/catalog/molochnye-produkty-syr-yaytsa/syry/syry-tverdye-i-polutverdye/?count=36&sort=price_asc&page={}', None, Cheese, None)


class FishFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://www.globus.ru/catalog/myaso-ryba-kulinariya/ryba-ikra-moreprodukty/kopchenaya-ryba/?count=36&sort=price_asc&page={}', None, Fish, None)


class VegetableFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://www.globus.ru/catalog/ovoshchi-frukty-zelen/?count=36&sort=price_asc&page={}', isVegetable, Vegetable, None)


class FruitFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://www.globus.ru/catalog/ovoshchi-frukty-zelen/?count=36&sort=price_asc&page={}', isFruit, Fruit, None)


class NutFetcher():
    def __init__(self):
        self.f1 = GenericProductFetcher(
            r'https://www.globus.ru/catalog/bakaleya/orekhi-sukhofrukty-semechki/orekhi/?count=36&sort=price_asc&page={}', isNut, Nut, None)

        self.f2 = GenericProductFetcher(
            r'https://www.globus.ru/catalog/bakaleya/orekhi-sukhofrukty-semechki/semechki/?count=36&sort=price_asc&page={}', isNut, Nut, None)

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()
        # return self.f2.fetch()


class WaterFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://www.globus.ru/catalog/napitki/limonady-toniki-gazirovannye-napitki/?page={}&sort=price_asc&count=36', None, None, Water)


class MilkyDrinkFetcher():
    def __init__(self):
        self.firstFetcher = GenericProductFetcher(
            r'https://www.globus.ru/catalog/molochnye-produkty-syr-yaytsa/moloko-slivki/moloko/?page={}&sort=price_asc&count=36', isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)

        self.secondFetcher = GenericProductFetcher(
            r'https://www.globus.ru/catalog/molochnye-produkty-syr-yaytsa/kislomolochnye-produkty/kefir/?page={}&sort=price_asc&count=36', isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)

        self.thirdFetcher = GenericProductFetcher(
            r'https://www.globus.ru/catalog/molochnye-produkty-syr-yaytsa/kislomolochnye-produkty/ryazhenka-prostokvasha-matsoni/?page={}&sort=price_asc&count=36', isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)

    def fetch(self):
        return self.firstFetcher.fetch() + self.secondFetcher.fetch() + self.thirdFetcher.fetch()

