import os
import subprocess
import datetime
import re

from food_parser.logger import *


# if __name__ == "__main__":
def getLastDump():
    dumpRegex = r'(\d{4})_(\d{2})_(\d{2}).dump$'
    dumpFolder = os.getenv('DB_DUMP_FOLDER', '/home/aggravator/Documents/foodpriceanalyzer/db_dumps')
    dumps = [f for f in os.listdir(dumpFolder) if re.search(dumpRegex,f.lower()) is not None]

    if len(dumps) == 0:
        return datetime.date(1993, 7, 5)

    dumpWithDates = []
    for df in dumps:
        match = re.search(dumpRegex, df.lower())
        dumpDate = datetime.date(int(match.group(1)), int(match.group(2)), int(match.group(3)))
        dumpWithDates.append((df, dumpDate))
    
    lastDumpDate = sorted(dumpWithDates, key=lambda x: x[1], reverse=True)[0][1]
    return lastDumpDate


def dumpDb():
    lastDumpDate = getLastDump()
    currentDate = datetime.datetime.now().date()
    dateDiff = currentDate - lastDumpDate

    dumpGap = int(os.getenv('DUMP_GAP', "7"))
    if dateDiff.days < dumpGap:
        logger.log(LogLevel.INFO, "Database is not dumped because {} has not yet passed since last dump".format(dumpGap))
        return

    dstDir = os.getenv('DB_DUMP_FOLDER', '/home/aggravator/Documents/foodpriceanalyzer/db_dumps')
    fileName = "productdb_" + datetime.date.today().isoformat().replace("-", "_")+".dump"
    absFileName = dstDir+os.path.sep+fileName
    
    subprocess.call(["pg_dump", "-Fc",
        "-U", os.getenv('DB_USER', 'postgres'),
        "-h", os.getenv('DB_HOST', '127.0.0.1'),
        os.getenv('DB_DATABASE', 'productdb'),
        "-f", absFileName],
        env={"PGPASSWORD": os.getenv('DB_PASSWORD', 'postgres')})
    logger.log(LogLevel.INFO, "Database is not dumped because {} has not yet passed since last dump".format(dumpGap))
