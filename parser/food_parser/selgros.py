import requests
import re

from food_parser.core import *
from food_parser.prodtype_criteria import *
from food_parser.logger import *


class GenericProductSelgrosFetcher:
    weight_regexp = [
        (r'([0-9]+[\.,]?[0-9]*)\s?(кг|КГ)', 1, 1000),
        (r'([0-9]+[\.,]?[0-9]*)\s?(г|г)', 1, 1),
        (r'([0-9]+[\.,]?[0-9]*)\s?(гр|ГР)', 1, 1)
    ]
    volume_regexp = [
        (r'([0-9]+[\.,]?[0-9]*)\s?[лЛ]', 1, 1000),
        (r'([0-9]+[\.,]?[0-9]*)\s?(мл|МЛ)', 1, 1)
    ]
    MAX_PAGE = 20

    def __init__(self, url, criterion, weightProductClass, volumeProductClass):
        self.url = url
        self.criterion = criterion
        self.weightProductClass = weightProductClass
        self.volumeProductClass = volumeProductClass

    def fetch(self):
        res = []
        for page in range(0, self.MAX_PAGE):
            resp = requests.get(self.url.format(page*30))
            humanLag()
            if not resp:
                logger.log(LogLevel.ERROR, "Failed to fetch product info by url {}".format(
                    resp.url))
                return None

            try:
                data = resp.json()
            except ValueError:
                logger.log(LogLevel.ERROR, "Failed to parse response {} fetched by url {}".format(
                    resp.text, self.url))
                return None

            if not data['data']['list']:
                break

            for result in data['data']['list']:
                rawName = result['name'].replace("®", "")

                if self.criterion and not self.criterion(rawName):
                    continue

                hasMatch = False
                productClass = self.weightProductClass
                if self.weightProductClass:
                    for rnw in name_weight_regexp:
                        regRes = re.search(rnw[0], rawName)
                        if regRes:
                            hasMatch = True
                            name = regRes.group(rnw[1][0]).strip(", ")
                            size = int(
                                float(regRes.group(rnw[1][1]).replace(",", "."))*rnw[2])
                            break

                    if not hasMatch:
                        name = rawName
                        weightStr = result['display_weight']
                        for rv in self.weight_regexp:
                            regRes = re.search(rv[0], weightStr)
                            if regRes:
                                hasMatch = True
                                size = int(
                                    float(regRes.group(rv[1]).replace(",", "."))*rv[2])
                                break

                if self.volumeProductClass and not hasMatch:
                    productClass = self.volumeProductClass
                    for rnw in name_volume_regexp:
                        regRes = re.search(rnw[0], rawName)
                        if regRes:
                            hasMatch = True
                            name = regRes.group(rnw[1][0]).strip(", ")
                            size = int(
                                float(regRes.group(rnw[1][1]).replace(",", "."))*rnw[2])
                            break

                    if not hasMatch:
                        name = rawName
                        for rv in self.volume_regexp:
                            regRes = re.search(rv[0], result['display_weight'])
                            if regRes:
                                hasMatch = True
                                size = int(
                                    float(regRes.group(rv[1]).replace(",", "."))*rv[2])
                                break

                if not hasMatch:
                    logger.log(LogLevel.WARNING,
                               "Failed to parse product {}".format(rawName))
                    continue

                try:
                    price = int(result['price']*100)
                except KeyError:
                    logger.log(
                        LogLevel.ERROR, 'Failed to find field price in {}'.format(result))
                    continue

                prod = productClass(name, size, price)
                prod.url = "https://igooods.ru/catalog/products/{}-{}".format(
                    result['model_id'], result['parameterize'])
                res.append(prod)

        return res


class SausageSelgrosFetcher():
    def __init__(self):
        self.f1 = GenericProductSelgrosFetcher(
            r'https://igooods.ru/api/v8/shops/163/categories/48/products?sort=price&sort_order=asc&offset={}', None, Sausage, None)
        self.f2 = GenericProductSelgrosFetcher(
            r'https://igooods.ru/api/v8/shops/163/categories/45/products?sort=price&sort_order=asc&offset={}', None, Sausage, None)

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()


class MeatSelgrosFetcher():
    def __init__(self):
        # Beef
        self.f1 = GenericProductSelgrosFetcher(r'https://igooods.ru/api/v8/shops/163/categories/38/products?sort=price&sort_order=asc&offset={}',
                                               None, Meat, None)
        # Pig
        self.f2 = GenericProductSelgrosFetcher(r'https://igooods.ru/api/v8/shops/163/categories/39/products?sort=price&sort_order=asc&offset={}',
                                               None, Meat, None)

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()


class BirdSelgrosFetcher(GenericProductSelgrosFetcher):
    def __init__(self):
        super().__init__(r'https://igooods.ru/api/v8/shops/163/categories/37/products?sort=price&sort_order=asc&offset={}',
                         None, Bird, None)


class CheeseSelgrosFetcher(GenericProductSelgrosFetcher):
    def __init__(self):
        super().__init__(r'https://igooods.ru/api/v8/shops/163/categories/34/products?sort=price&sort_order=asc&offset={}',
                         None, Cheese, None)


class FishSelgrosFetcher(GenericProductSelgrosFetcher):
    def __init__(self):
        super().__init__(r'https://igooods.ru/api/v8/shops/163/categories/53/products?sort=price&sort_order=asc&offset={}',
                         None, Fish, None)


class VegetableSelgrosFetcher(GenericProductSelgrosFetcher):
    def __init__(self):
        super().__init__(r'https://igooods.ru/api/v8/shops/163/categories/1/products?sort=price&sort_order=asc&offset={}',
                         isVegetable, Vegetable, None)


class FruitSelgrosFetcher(GenericProductSelgrosFetcher):
    def __init__(self):
        super().__init__(r'https://igooods.ru/api/v8/shops/163/categories/1/products?sort=price&sort_order=asc&offset={}', isFruit, Fruit, None)


class NutSelgrosFetcher(GenericProductSelgrosFetcher):
    def __init__(self):
        super().__init__(r'https://igooods.ru/api/v8/shops/163/categories/23/products?sort=price&sort_order=asc&offset={}', isNut, Nut, None)


def isWater(name):
    upName = name.upper()
    return (upName.find("НАПИТОК") > -1 or upName.find("НАПИТ.") > -1) and upName.find("ПИВН") == -1


class WaterSelgrosFetcher(GenericProductSelgrosFetcher):
    def __init__(self):
        super().__init__(r'https://igooods.ru/api/v8/shops/163/categories/104/products?sort=price&sort_order=asc&offset={}',
                         isWater, None, Water)


class MilkyDrinkSelgrosFetcher():
    def __init__(self):
        self.f1 = GenericProductSelgrosFetcher(r'https://igooods.ru/api/v8/shops/163/categories/25/products?sort=price&sort_order=asc&offset={}',
                                               isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)
        self.f2 = GenericProductSelgrosFetcher(r'https://igooods.ru/api/v8/shops/163/categories/30/products?sort=price&sort_order=asc&offset={}',
                                               isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()
