import requests
import re
import bs4
from food_parser.core import *
from food_parser.prodtype_criteria import *
from food_parser.logger import *

da_weight_regexp = [
    (r'(\d+)\s?[гГ]', 1, 1),
    (r'(цена за)?\s*(\d+)\s?(кг|КГ)', 2, 1000),
]

da_volume_regexp = [
    (r'([0-9]+[\.,]?[0-9]*)\s?[лЛ]', 1, 1000)
]


class GenericProductFetcher:

    def __init__(self, url, criterion, weightProductClass, volumeProductClass):
        self.url = url
        self.criterion = criterion
        self.weightProductClass = weightProductClass
        self.volumeProductClass = volumeProductClass

    def fetch(self):
        res = None

        resp = requests.get(self.url)
        humanLag()

        if not resp:
            logger.log(LogLevel.ERROR, "Failed to fetch info by url {}".format(
                self.url))
            return res

        res = []
        tree = bs4.BeautifulSoup(resp.text, 'lxml')
        items = tree.findAll("div", {"class": "catalog-item1"})
        for item in items:
            name = item.find(
                "div", {"class": "description"}).find("h6").text.strip("\n ")

            if self.criterion and not self.criterion(name):
                continue

            rawSize = item.find(
                "div", {"class": "description"}).find("p").text

            hasMatch = False
            productClass = self.weightProductClass
            if self.weightProductClass:
                for wr in da_weight_regexp:
                    regRes = re.search(wr[0], rawSize)
                    if regRes:
                        hasMatch = True
                        size = int(
                            float(regRes.group(wr[1]).replace(",", "."))*wr[2])
                        break
            if not hasMatch and self.volumeProductClass:
                productClass = self.volumeProductClass
                for vr in da_volume_regexp:
                    regRes = re.search(vr[0], rawSize)
                    if regRes:
                        hasMatch = True
                        size = int(
                            float(regRes.group(vr[1]).replace(",", "."))*vr[2])
                        break

            if not hasMatch:
                logger.log(
                    LogLevel.WARNING, "Failed to parse product weight {}".format(rawSize))
                continue

            priceEl = item.find("div", {"class": "price1 bfirst"})
            tempPriceRaw = (priceEl.find(
                "strong").text+"."+priceEl.find("b").text).replace("*", "")
            price = int(float(re.sub(r'\s', "", tempPriceRaw))*100)

            product = productClass(name, size, price)
            # product.url = item.attrs["href"]
            res.append(product)

        return res


class SausageFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://market-da.ru/promo.html', isSausage, Sausage, None)


meatKeys = ["свинина", "свиная", "свиной", "свиное", "свиные",
            "говяжья", "говядина", "говяжий", "говядины"]


def isMeat(name):
    lowName = name.lower()
    for key in meatKeys:
        if re.search(r"([^а-я]|^){}([^а-я]|$)".format(key), lowName):
            return True
    return False


class MeatFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://market-da.ru/promo.html', isMeat, Meat, None)


class BirdFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://market-da.ru/promo.html', isBird, Bird, None)


class CheeseFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://market-da.ru/promo.html', isCheese, Cheese, None)


class VegetableFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://market-da.ru/promo.html', isVegetable, Vegetable, None)


class FruitFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://market-da.ru/promo.html', isFruit, Fruit, None)


class NutFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://market-da.ru/promo.html', isNut, Nut, None)


class WaterFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://market-da.ru/promo.html', lambda name: name.lower().find("напиток") > -1, None, Water)


class MilkyDrinkFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://market-da.ru/promo.html', isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)


if __name__ == '__main__':
    data = SausageFetcher().fetch()
    jh = 3