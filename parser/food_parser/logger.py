import enum
import datetime


class LogLevel(enum.Enum):
    WARNING = 1
    ERROR = 2
    INFO = 3


class StreamLogger:
    def log(self, level, message):
        print(datetime.datetime.now(), level, message[:200])


class FileLogger:
    def __init__(self, filePath):
        self.filePath = filePath

    def log(self, level, message):
        print(datetime.datetime.now(), level, message.replace(
            "\n", "\\n"), file=open(self.filePath, "a"))


logger = StreamLogger()
# logger = FileLogger(os.path.dirname(os.path.abspath(__file__))+"/log.txt")