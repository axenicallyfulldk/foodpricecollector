import requests
import re
import bs4
from urllib.parse import urljoin
from food_parser.core import *
from food_parser.logger import *
from food_parser.prodtype_criteria import *


class GenericProductFetcher:

    MAX_PAGE = 20

    def __init__(self, urlTemplate, criterion=None, weightProductClass=None, volumeProductClass=None):
        self.urlTemplate = urlTemplate
        self.criterion = criterion
        self.weightProductClass = weightProductClass
        self.volumeProductClass = volumeProductClass

    def fetch(self):
        res = []
        prodsPerPageCookie = {'limit': '100'}
        for pi in range(1, self.MAX_PAGE):
            pageUrl = self.urlTemplate.format(pi)
            resp = requests.get(pageUrl, cookies = prodsPerPageCookie)

            humanLag(10)

            if not resp:
                logger.log(LogLevel.ERROR, "Failed to fetch info by url {}".format(
                    pageUrl))
                return res

            pageRes = self.__parseResponse(resp)

            res += pageRes

            tree = bs4.BeautifulSoup(resp.text, 'lxml')
            nextButton = tree.find("span", {"class": "more_text_ajax"})
            if not nextButton or nextButton.hidden == True:
                break

        return res

    def __parseResponse(self, resp):
        tree = bs4.BeautifulSoup(resp.text, 'lxml')

        items = tree.findAll("div", {"class": "catalog_item_wrapp"})

        res = []
        for item in items:
            nameEl = item.find(
                "div", {"class": "item-title"})
            if not nameEl:
                logger.log(
                    LogLevel.WARNING, "Dixy Failed to find element that contains product's name {}".format(item))
            nameRaw = nameEl.text.strip("\n ")

            priceEl = item.find("span", {"class": "price_value"})
            if not priceEl:
                continue

            priceRaw = re.sub(r'\s', "", priceEl.text)
            price = int(float(priceRaw)*100)

            portionEl = item.find("span", {"class": "price_measure"})
            portion = portionEl.text.replace("/", "") if portionEl else None

            if self.criterion is not None and not self.criterion(nameRaw):
                continue

            prodClass = self.weightProductClass
            hasMatch = False
            if portion and "КГ" == portion.upper():
                hasMatch = True
                name = nameRaw
                size = 1000
            else:                
                name, size = extractNameAndWeight(nameRaw)
                hasMatch = name is not None and size is not None

            if not hasMatch:
                prodClass = self.volumeProductClass                
                name, size = extractNameAndVolume(nameRaw)
                hasMatch = name is not None and size is not None

            if not hasMatch:
                logger.log(
                    LogLevel.WARNING, "Failed to find a match for product {}".format(nameRaw))
                continue

            product = prodClass(name, size, price)
            product.url = urljoin(resp.url, item.find(
                "div", {"class": "item-title"}).find("a").attrs["href"])
            res.append(product)

        return res


class GenericProductFetcher2:

    MAX_PAGE = 20

    def __init__(self, urlTemplate, sectionID, productCriterion, weightProductClass, volumeProductClass):
        self.urlTemplate = urlTemplate
        self.sectionID = sectionID
        self.productCriterion = productCriterion
        self.weightProductClass = weightProductClass
        self.volumeProductClass = volumeProductClass

    def fetch(self):
        res = []
        self.handledProducts = {}
        reqBody = {'FILTER[SECTION_ID][]':self.sectionID}
        for pi in range(1, self.MAX_PAGE):
            pageUrl = self.urlTemplate.format(pi)
            resp = requests.post(pageUrl, data = reqBody)

            humanLag()

            if not resp:
                logger.log(LogLevel.ERROR, "Failed to fetch info by url {}".format(
                    pageUrl))
                return res

            pageRes = self.__parseResponse(resp)
            if not pageRes:
                break

            res += pageRes

            tree = bs4.BeautifulSoup(resp.text, 'lxml')
            if not tree.find("a", {"class": "btn view-more"}):
                break

        return res

    def __parseResponse(self, resp):
        tree = bs4.BeautifulSoup(resp.text, 'lxml')

        items = list(set(tree.findAll(
            "div", {"class": "item"}))-set(tree.findAll("div", {"class": "item empty"})))

        res = []
        for item in items:
            prodId = list(item.children)[0]
            # nameEl = item.find("p", {"class": "name"})
            nameEl = item.find("div", {"class": "dixyCatalogItem__hover"})
            if not nameEl:
                nameEl = item.find("div", {"class": "dixyModal__title"})
                if not nameEl:
                    logger.log(
                        LogLevel.WARNING, "Dixy Failed to find name element of the product {}".format(item))
                continue
            nameRaw = nameEl.text.strip("\n ")

            if self.productCriterion and not self.productCriterion(nameRaw):
                continue

            priceEl = item.find("p", {"itemprop": "price"})
            if not priceEl:
                continue
            priceRaw = priceEl["content"]

            prodClass = self.weightProductClass

            name, size = extractNameAndWeight(nameRaw)
            hasMatch = name is not None and size is not None

            if not hasMatch:
                prodClass = self.volumeProductClass
                name, size = extractNameAndVolume(nameRaw)
                hasMatch = name is not None and size is not None

            if not hasMatch:
                logger.log(LogLevel.WARNING,
                           "Dixy Failed to parse product's name {}".format(nameRaw))
                continue

            price = int(float(priceRaw)*100)

            product = prodClass(name, size, price)
            # product.url = item.find("div", {"class": "item-title"}).find("a").attrs["href"]
            self.handledProducts[prodId] = True
            res.append(product)

        return res


class SausageFetcher():
    def __init__(self):
        # Sausage
        self.f1 = GenericProductFetcher(r'https://dostavka.dixy.ru/catalog/myaso_ptitsa_kolbasy/kolbasa_vetchina_1/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N',
                                            None, Sausage)
        # Weenie
        self.f2 = GenericProductFetcher(r'https://dostavka.dixy.ru/catalog/myaso_ptitsa_kolbasy/sosiski_sardelki_1/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N',
                                            None, Sausage)

    def fetch(self):
        res1 = self.f1.fetch()
        res2 = self.f2.fetch()
        return res1 + res2


class SausageFetcher2(GenericProductFetcher2):
    def __init__(self):
        super().__init__(r'https://dixy.ru/catalog/?PAGEN_1={}', 1407,
                         lambda name: (name.upper().find("КОЛБАСА") >= 0), Sausage, None)


class MeatFetcher():
    def __init__(self):
        # Beef
        self.f1 = GenericProductFetcher(r'https://dostavka.dixy.ru/catalog/myaso_ptitsa_kolbasy/govyadina_baranina/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N',
                                            None, Meat)
        # Pig
        self.f2 = GenericProductFetcher(r'https://dostavka.dixy.ru/catalog/myaso_ptitsa_kolbasy/svinina_1/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N',
                                            None, Meat)

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()


class BirdFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://dostavka.dixy.ru/catalog/myaso_ptitsa_kolbasy/ptitsa_krolik/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N',
                         isBird, Bird, None)


class CheeseFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://dostavka.dixy.ru/catalog/moloko_syr_yaytsa/syry/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N', None, Cheese)


class CheeseFetcher2(GenericProductFetcher2):
    def __init__(self):
        super().__init__(r'https://dixy.ru/catalog/?PAGEN_1={}', 1408,
                         lambda name: (name.upper().find("СЫР") >= 0), Cheese, None)


class FishFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://dostavka.dixy.ru/catalog/ryba_moreprodukty_ikra/solenaya_i_kopchenaya_ryba/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N', None, Fish)


class VegetableFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://dostavka.dixy.ru/catalog/ovoshchi_frukty_zelen_griby_/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N', isVegetable, Vegetable)


class VegetableFetcher2(GenericProductFetcher2):
    def __init__(self):
        super().__init__(r'https://dixy.ru/catalog/?PAGEN_1={}', 1592,
                         isVegetable, Vegetable, None)


class FruitFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://dostavka.dixy.ru/catalog/ovoshchi_frukty_zelen_griby_/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N', isFruit, Fruit)


class FruitFetcher2(GenericProductFetcher2):
    def __init__(self):
        super().__init__(r'https://dixy.ru/catalog/?PAGEN_1={}', 1592,
                         isFruit, Fruit, None)


class NutFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://dostavka.dixy.ru/catalog/sousy_spetsii_orekhi/orekhi_sukhofrukty_semechki/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N', isNut, Nut)


class NutFetcher2(GenericProductFetcher2):
    def __init__(self):
        super().__init__(r'https://dixy.ru/catalog/?PAGEN_1={}', 1589,
                         isNut, Nut, None)


class WaterFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://dostavka.dixy.ru/catalog/voda_soki_napitki_1/gazirovannye_napitki_limonady/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N', None, None, Water)


class WaterFetcher2(GenericProductFetcher2):
    def __init__(self):
        super().__init__(r'https://dixy.ru/catalog/?PAGEN_1={}', 1401,
                         lambda name: (name.upper().find("НАПИТОК") >= 0), None, Water)


class MilkyDrinkFetcher:
    def __init__(self):
        self.firstFetcher = GenericProductFetcher(
            r'https://dostavka.dixy.ru/catalog/moloko_syr_yaytsa/moloko_slivki/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N', isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)
        self.secondFetcher = GenericProductFetcher(
            r'https://dostavka.dixy.ru/catalog/moloko_syr_yaytsa/kislomolochnye_produkty/?PAGEN_1={}&ajax_get=Y&AJAX_REQUEST=Y&bitrix_include_areas=N', isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)

    def fetch(self):
        products1 = self.firstFetcher.fetch()
        products2 = self.secondFetcher.fetch()
        prodList1 = products1 if isinstance(products1, list) else []
        prodList2 = products2 if isinstance(products2, list) else []
        return prodList1 + prodList2


class MilkyDrinkFetcher2(GenericProductFetcher2):
    def __init__(self):
        super().__init__(r'https://dixy.ru/catalog/?PAGEN_1={}', 1409,
                         isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)

if __name__ == '__main__':
    data = SausageFetcher().fetch()
    jk = 3