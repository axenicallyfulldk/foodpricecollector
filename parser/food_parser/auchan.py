from os import name
import requests
from food_parser.core import *
from food_parser.prodtype_criteria import *
from food_parser.logger import *


class GenericProductFetcher:

    MAX_PAGE = 20

    def __init__(self, urlTemplate, category, criterion=None, weightProductClass=None, volumeProductClass=None):
        self.urlTemplate = urlTemplate
        self.category = category
        self.criterion = criterion
        self.weightProductClass = weightProductClass
        self.volumeProductClass = volumeProductClass

    def fetch(self):
        res = []
        payload = {"filter":{"category":self.category,"active_only":True}}
        for pi in range(1, self.MAX_PAGE):
            pageUrl = self.urlTemplate.format(pi)
            resp = requests.post(pageUrl, json = payload)
            humanLag()

            if not resp:
                logger.log(LogLevel.ERROR, "Failed to fetch info by url {}".format(
                    pageUrl))
                return res
            
            try:
                data = resp.json()
            except ValueError:
                logger.log(LogLevel.ERROR, "Failed to parse response {} fetched by url {}".format(
                    resp.text, self.url))
                return None
            
            if len(data['items']) == 0:
                break

            pageRes = self.__parseResponse(data)

            res += pageRes

        return res

    def __parseResponse(self, resp):
        res = []
        for item in resp['items']:
            nameRaw = item['title']

            if self.criterion is not None and not self.criterion(nameRaw):
                continue

            weightProp = next((x for x in item['characteristics'] if x['title'] == 'Масса нетто, кг'), None)
            volumeProp = next((x for x in item['characteristics'] if x['title'] == 'Объем, л'), None)

            if weightProp is None and volumeProp is None:
                logger.log(LogLevel.WARNING, 'Auchan Failed to find weight or volume of product "{}"'.format(nameRaw))
                continue

            if weightProp is None or volumeProp is None:
                if weightProp is not None and self.weightProductClass is None:
                    if re.search(r'\d\s*[лЛ]$', nameRaw) is None and re.search(r'\d\s*[мМ][лЛ]$', nameRaw) is None: 
                        logger.log(LogLevel.WARNING, 'Auchan Product "{}" is weight product, but weight product class was not provided'.format(nameRaw))
                        continue

                    volumeProp = weightProp
                    weightProp = None

                if volumeProp is not None and self.volumeProductClass is None:
                    if re.search(r'\d\s*[гГ]$', nameRaw) is None: 
                        logger.log(LogLevel.WARNING, 'Auchan Product "{}" is weight product, but weight product class was not provided'.format(nameRaw))
                        continue
                    logger.log(LogLevel.WARNING, 'Auchan Product "{}" is volume product, but volume product class was not provided'.format(nameRaw))
                    continue

            if weightProp is not None and self.weightProductClass is not None:
                weightGramm = int(float(weightProp['value'])*1000)
                if item['price']['per'] == 'weight_kg':
                    pricePerKg = item['price']['value']
                    pricePenny = int(float(pricePerKg*weightGramm/1000)*100)
                elif item['price']['per'] == 'item':
                    pricePenny = int(item['price']['value']*100)
                else:
                    logger.log(LogLevel.WARNING, 'Auchan Failed to find price for product "{}"'.format(nameRaw))
                    continue

                product = self.weightProductClass(nameRaw, weightGramm, pricePenny)

            elif volumeProp is not None and self.volumeProductClass is not None:
                volumeMl = int(float(volumeProp['value'])*1000)
                if item['price']['per'] == 'item':
                    pricePenny = int(float(item['price']['value'])*100)
                else:
                    logger.log(LogLevel.WARNING, 'Auchan Failed to find price for product "{}"'.format(nameRaw))
                    continue

                product = self.volumeProductClass(nameRaw, volumeMl, pricePenny)

            product.url = 'https://www.auchan.ru/product/{}/'.format(item['code'])
            res.append(product)

        return res


class SausageFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://www.auchan.ru/v1/catalog/products?merchantId=1&page={}&perPage=40&orderField=price&orderDirection=asc', 'kolbasy-vetchina-1',
                         lambda name: (name.upper().find("КОЛБАСА") >= 0), Sausage, None)

class MeatFetcher():
    def __init__(self):
        # Beef
        self.f1 = GenericProductFetcher(r'https://www.auchan.ru/v1/catalog/products?merchantId=1&page={}&perPage=40&orderField=price&orderDirection=asc', 'govyadina_telyatina_1',
                                            None, Meat)
        # Pig
        self.f2 = GenericProductFetcher(r'https://www.auchan.ru/v1/catalog/products?merchantId=1&page={}&perPage=40&orderField=price&orderDirection=asc', 'svinina_3',
                                            None, Meat)

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()



class BirdFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://www.auchan.ru/v1/catalog/products?merchantId=1&page={}&perPage=40&orderField=price&orderDirection=asc', 'ptica',
                         isBird, Bird, None)

class CheeseFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://www.auchan.ru/v1/catalog/products?merchantId=1&page={}&perPage=40&orderField=price&orderDirection=asc', 'tverdyy', None, Cheese)

class FishFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(
            r'https://www.auchan.ru/v1/catalog/products?merchantId=1&page={}&perPage=40&orderField=price&orderDirection=asc', 'ryba_kopchenaya', None, Fish)


class VegetableFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://www.auchan.ru/v1/catalog/products?merchantId=1&page={}&perPage=40&orderField=price&orderDirection=asc', 'ovoschi', isVegetable, Vegetable)

class FruitFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://www.auchan.ru/v1/catalog/products?merchantId=1&page={}&perPage=40&orderField=price&orderDirection=asc', 'frukty-yagody', isFruit, Fruit)


class NutFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://www.auchan.ru/v1/catalog/products?merchantId=1&page={}&perPage=40&orderField=price&orderDirection=asc', 'orehi-suhofrukty-semechki', isNut, Nut)

class WaterFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://www.auchan.ru/v1/catalog/products?merchantId=1&page={}&perPage=40&orderField=price&orderDirection=asc', 'limonady_gazirovannye_napitki', None, None, Water)

class MilkyDrinkFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://www.auchan.ru/v1/catalog/products?merchantId=1&page={}&perPage=40&orderField=price&orderDirection=asc', 'moloko', isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)

if __name__ == '__main__':
    prods = NutFetcher().fetch()
    pass