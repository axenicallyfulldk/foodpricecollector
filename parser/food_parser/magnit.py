import requests
import re
import bs4
from urllib.parse import urljoin

from food_parser.core import *
from food_parser.logger import *
from food_parser.prodtype_criteria import *


class GenericProductFetcher:

    MAX_PAGE = 10

    def __init__(self, urlTemplate, criterion, weightProductClass, volumeProductClass):
        self.urlTemplate = urlTemplate
        self.criterion = criterion
        self.weightProductClass = weightProductClass
        self.volumeProductClass = volumeProductClass

    def fetch(self):
        res = None
        resp = requests.get(self.urlTemplate)
        humanLag()

        if not resp:
            logger.log(LogLevel.ERROR, "Failed to fetch info by url {}".format(
                self.urlTemplate))
            return res

        pageRes = self.__parseResponse(resp)

        if res is None:
            res = pageRes
        else:
            res += pageRes

        return res

    def __parseResponse(self, resp):
        tree = bs4.BeautifulSoup(resp.text, 'lxml')

        items = tree.findAll("a", {"class": "card-sale"})

        res = []
        for item in items:
            nameEl = item.find("div", {"class": "card-sale__title"})
            if not nameEl:
                logger.log(LogLevel.WARNING,
                           "Magnit Failed to find name element {}".format(item))
                continue

            nameRaw = nameEl.text.strip("\n ")

            if self.criterion and not self.criterion(nameRaw):
                continue

            priceEl = item.find(
                "div", {"class": "label__price_new"})

            if not priceEl:
                continue

            tempPriceRaw = re.sub(r'(\d)\s(\d)', r'\1.\2', priceEl.text)
            priceRaw = re.sub(r'\s', "", tempPriceRaw)

            prodClass = self.weightProductClass
            hasMatch = False
            if self.weightProductClass:
                name, size = extractNameAndWeight(nameRaw)
                hasMatch = name is not None and size is not None

            if not hasMatch and self.volumeProductClass:
                prodClass = self.volumeProductClass
                name, size = extractNameAndVolume(nameRaw)
                hasMatch = name is not None and size is not None

            if not hasMatch:
                logger.log(
                    LogLevel.WARNING, 'Magnit Failed to parse product name "{}"'.format(nameRaw))
                continue

            price = int(float(priceRaw)*100)

            product = prodClass(name, size, price)
            product.url = urljoin(resp.url, item.attrs["href"])
            res.append(product)

        return res


class SausageFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://magnit.ru/promo/?format[]=mm&category[]=myaso_ryba&sort=PRICE_ASC', isSausage, Sausage, None)


class CheeseFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://magnit.ru/promo/?format[]=mm&category[]=molochnie_produkty&sort=PRICE_ASC', isCheese, Cheese, None)


class VegetableFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://magnit.ru/promo/?format[]=mm&category[]=fruits_vegetables&sort=PRICE_ASC', isVegetable, Vegetable, None)


class FruitFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://magnit.ru/promo/?format[]=mm&category[]=fruits_vegetables&sort=PRICE_ASC', isFruit, Fruit, None)


class NutFetcher(GenericProductFetcher):
    def __init__(self):
        # https://magnit.ru/promo/?FILTER=1&SORT=PRICE_ASC&FORMAT%5B%5D=1&CATEGORY%5B%5D=70
        super().__init__(r'https://magnit.ru/promo/?format[]=mm&category[]=sneki&sort=PRICE_ASC', isNut, Nut, None)


def isWater(name):
    upperName = name.upper()
    return upperName.find("НАПИТОК") >= 0


class WaterFetcher(GenericProductFetcher):
    def __init__(self):
        super(WaterFetcher, self).__init__(r'https://magnit.ru/promo/?format[]=mm&category[]=napitki&sort=PRICE_ASC', isWater, None, Water)
        


class MilkyDrinkFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://magnit.ru/promo/?format[]=mm&category[]=molochnie_produkty&sort=PRICE_ASC', isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)
        


if __name__ == '__main__':
    data = CheeseFetcher().fetch()
    gh = 3