import requests
import re

from food_parser.core import *
from food_parser.logger import *
from food_parser.prodtype_criteria import *


class GenericProductFetcher:
    MAX_PAGE = 20

    def __init__(self, url, productCriterion, weightProductClass, volumeProductClass):
        self.url = url
        self.productCriterion = productCriterion
        self.weightProductClass = weightProductClass
        self.volumeProductClass = volumeProductClass

    def fetch(self):
        res = []
        for page in range(1, self.MAX_PAGE):
            resp = requests.get(self.url.format(page))
            humanLag()
            if not resp:
                logger.log(LogLevel.ERROR, "Failed to fetch product info by url {}".format(
                    resp.url))
                return None

            try:
                data = resp.json()
            except ValueError:
                logger.log(LogLevel.ERROR, "Failed to parse response {} fetched by url {}".format(
                    resp.text, self.url))
                return None

            for result in data['results']:
                hasMatch = False
                productClass = self.weightProductClass

                if self.productCriterion and not self.productCriterion(result['name']):
                    continue

                if self.weightProductClass:
                    for rnw in name_weight_regexp:
                        regRes = re.search(rnw[0], result['name'])
                        if regRes:
                            hasMatch = True
                            name = regRes.group(rnw[1][0]).strip(", ")
                            size = int(
                                float(regRes.group(rnw[1][1]).replace(",", "."))*rnw[2])
                            break

                if not hasMatch and self.volumeProductClass:
                    productClass = self.volumeProductClass
                    for rnw in name_volume_regexp:
                        regRes = re.search(rnw[0], result['name'])
                        if regRes:
                            hasMatch = True
                            name = regRes.group(rnw[1][0]).strip(", ")
                            size = int(
                                float(regRes.group(rnw[1][1]).replace(",", "."))*rnw[2])
                            break

                if not hasMatch:
                    logger.log(
                        LogLevel.WARNING, 'Pyatorochka Failed to parse product name "{}"'.format(result['name']))
                    continue

                try:
                    price = int(result['current_prices']
                                ['price_promo__min']*100)
                except KeyError:
                    logger.log(
                        LogLevel.ERROR, 'Pyatorochka Failed to find field current_prices.price_promo__min in {}'.format(result))
                    continue

                prod = productClass(name, size, price)
                prod.url = "https://5ka.ru/special_offers/{}".format(
                    result["id"])
                res.append(prod)

            if not data["next"]:
                break

        return res


class SausageFetcher():
    def __init__(self):
        # Sausages
        self.f1 = GenericProductFetcher(
            r'https://5ka.ru/api/v2/special_offers/?store=&records_per_page=50&page={}&categories=628&ordering=&price_promo__gte=&price_promo__lte=&search=', None, Sausage, None)

    def fetch(self):
        return self.f1.fetch()


class CheeseFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://5ka.ru/api/v2/special_offers/?store=&records_per_page=50&page={}&categories=699&ordering=&price_promo__gte=&price_promo__lte=&search=', None, Cheese, None)


class VegetableFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://5ka.ru/api/v2/special_offers/?store=&records_per_page=50&page={}&categories=PUI37&ordering=price_promo_min&price_promo__gte=&price_promo__lte=&search=', isVegetable, Vegetable, None)


class FruitFetcher(GenericProductFetcher):
    def __init__(self):
        self.f1 = GenericProductFetcher(
            r'https://5ka.ru/api/v2/special_offers/?store=&records_per_page=50&page={}&categories=PUI1&ordering=price_promo_min&price_promo__gte=&price_promo__lte=&search=', isFruit, Fruit, None)
        self.f2 = GenericProductFetcher(
            r'https://5ka.ru/api/v2/special_offers/?store=&records_per_page=50&page={}&categories=PUI43&ordering=price_promo_min&price_promo__gte=&price_promo__lte=&search=', isFruit, Fruit, None)

    def fetch(self):
        return self.f1.fetch() + self.f2.fetch()


class NutFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://5ka.ru/api/v2/special_offers/?store=&records_per_page=50&page={}&categories=871&ordering=&price_promo__gte=&price_promo__lte=&search=', None, Nut, None)


def isWater(name):
    upName = name.upper()
    return (upName.find("НАПИТОК") > -1 or upName.find("НАПИТ.") > -1) and upName.find("ПИВН") == -1


class WaterFetcher(GenericProductFetcher):
    def __init__(self):
        super().__init__(r'https://5ka.ru/api/v2/special_offers/?store=&records_per_page=50&page={}&categories=732&ordering=&price_promo__gte=&price_promo__lte=&search=',
                         isWater, None, Water)


class MilkyDrinkFetcher():
    def __init__(self):
        self.f1 = GenericProductFetcher(r'https://5ka.ru/api/v2/special_offers/?store=&records_per_page=50&page={}&categories=698&ordering=&price_promo__gte=&price_promo__lte=&search=',
                                                   isMilkyDrink, MilkyDrinkWeight, MilkyDrinkVolume)

    def fetch(self):
        return self.f1.fetch()

if __name__ == '__main__':
    data = SausageFetcher().fetch()
    gh = 3