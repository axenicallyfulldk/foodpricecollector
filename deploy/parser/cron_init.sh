#!/bin/bash

# We have to copy environmental variables because cron runned programs would not
# see them otherwise.
printenv | grep -v "no_proxy" > /etc/environment

touch /var/log/cron.log
crontab /etc/cron.d/cron_task