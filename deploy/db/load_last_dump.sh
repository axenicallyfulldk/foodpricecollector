#!/bin/bash

# Find dump files
DUMPFILES=`find /db_dumps/*_????_??_??.dump -type f`
# Extract dump files' date
DUMPFILES_DATES=`printf "%s\n" "${DUMPFILES[@]}" | grep -oP '[\d]+_[\d]+_[\d]+'`
# Concat dump files' names with their dates
DUMPFILES_WITH_DATES=`paste <(printf "%s\n" "${DUMPFILES[@]}") <(printf "%s\n" "${DUMPFILES_DATES[@]}")`
# Sort dump files
DUMPFILES_SORTED=`printf "%s\n" "${DUMPFILES_WITH_DATES[@]}" | sort -rk2`
# Extract latest dump file
DUMPFILE=`printf "%s\n" "${DUMPFILES_SORTED[@]}" | head -n 1 | awk '{ print $1 }'`

# echo $DUMPFILE
# Restore database
# psql -U $POSTGRES_USER -f $DUMPFILE -d $POSTGRES_DB
pg_restore -U $POSTGRES_USER -d $POSTGRES_DB $DUMPFILE 
