package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"time"
	"web/internal/controllers"
	"web/internal/producttype"
	"web/internal/shop"
	"web/internal/snapshot"
	"web/internal/volumeproduct"
	"web/internal/weightproduct"

	"go.uber.org/zap"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

func getPostgresDb(dataSourceName string) (*sql.DB, error) {
	db, err := sql.Open("postgres", dataSourceName)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return db, nil
}

func JSONContent(next http.Handler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func main() {
	zapLogger, _ := zap.NewProduction()
	defer zapLogger.Sync() // flushes buffer
	logger := zapLogger.Sugar()

	psqlInfo := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		getEnv("DB_HOST", "127.0.0.1"), getEnvAsInt("DB_PORT", 5432),
		getEnv("DB_USERNAME", "postgres"), getEnv("DB_PASSWORD", "postgres"),
		getEnv("DB_DATABASE", "productdb"))

	dbConn, err := getPostgresDb(psqlInfo)

	dbConnAttempt := 1
	for err != nil {
		logger.Infow(
			fmt.Sprintf("Failed to connect to db, attempt #%d", dbConnAttempt),
			"type", "DB")
		time.Sleep(2 * time.Second)
		if dbConnAttempt >= 10 {
			return
		}
		dbConn, err = getPostgresDb(psqlInfo)
		dbConnAttempt++
	}

	// Create repositories
	ptRepo := producttype.NewProductTypeRepo(logger)
	weightRepo := weightproduct.NewWeightProductPostgresRepo(dbConn, logger)
	volumeRepo := volumeproduct.NewVolumeProductPostgresRepo(dbConn, logger)
	shopRepo := shop.NewShopRepo(dbConn, logger)
	snapRepo := snapshot.NewSnapshotRepo(dbConn, logger)

	// Create controllers
	ptController := controllers.NewProductTypeController(ptRepo, logger)
	wpController := controllers.NewWeightProductController(weightRepo, logger)
	vpController := controllers.NewVolumeProductController(volumeRepo, logger)
	shopController := controllers.NewShopController(shopRepo, logger)
	snapController := controllers.NewSnapshotController(snapRepo, logger)

	// Create router
	r := mux.NewRouter()

	// Init api end-points
	restRouter := gin.Default()

	restRouter.GET("/product-types", ptController.Index)

	restRouter.GET("/weight-products", wpController.Index)

	restRouter.GET("/volume-products", vpController.Index)

	restRouter.GET("/shops", shopController.Index)
	restRouter.GET("/shops/:id", shopController.Show)

	restRouter.GET("/snapshots", snapController.Index)
	restRouter.GET("/snapshots/last", snapController.Last)
	restRouter.GET("/snapshots/:id", snapController.Show)

	r.PathPrefix("/api").Handler(JSONContent(http.StripPrefix("/api", restRouter)))

	r.PathPrefix("/").Handler(http.FileServer(http.Dir("../../frontend/dist")))

	addr := ":" + getEnv("PORT", "8080")
	logger.Infow("starting server",
		"type", "START",
		"addr", addr,
	)

	http.ListenAndServe(addr, r)
}
