import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// import WeightProductList from './components/WeightProductList'
// import VolumeProductList from './components/VolumeProductList'
import ProductList from './components/ProductList'

Vue.use(BootstrapVue)

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}

import * as rep from './repositories'
window.ptRepo = rep.getProductTypeRepo();
window.shopRepo = rep.getShopRepo();
window.snapRepo = rep.getSnapRepo();
window.wpRepo = rep.getWeightProductRepo();
window.vpRepo = rep.getVolumeProductRepo();

Vue.config.productionTip = false

new Vue({
  render: h => h(ProductList),
}).$mount('#app')
