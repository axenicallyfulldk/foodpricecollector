import axios from "axios";

function axiosRespHandler(axiosPromise){
    return new Promise((resolve, reject) => {
        axiosPromise
            .then(resp => resolve(resp.data))
            .catch(err => reject(err));
    });
}

function mergeParams(params1, params2){
    for(const pair of params2.entries()){
        params1.append(pair[0], pair[1]);
    }
    return params1;
}

// Builds two requests: get all items, get item by id.
class BaseRequestBuilder {
    constructor(url){
        this.baseUrl = url.replace(/\/+$/g, "")

        let u = new URL(this.baseUrl, document.baseURI);
        this.baseUrlWithouParams = u.protocol+'//'+u.host+u.pathname;

        this.baseUrlParams = u.searchParams;
    }

    getList(params){
        let newParams = mergeParams(new URLSearchParams(params), this.baseUrlParams);
        let paramString = newParams.toString();
        return this.baseUrlWithouParams + (paramString != "" ? "?" : "") +
            newParams.toString();
    }

    getById(id){
        return `${this.baseUrl}/${id}`
    }
}

class BaseRepo {
    constructor(reqBuilder){
        this.reqBuilder = reqBuilder
    }

    async getList(params){
        return axiosRespHandler(axios.get(this.reqBuilder.getList(params)))
    }

    async getById(id){
        return axiosRespHandler(axios.get(this.reqBuilder.getById(id)));
    }
}

class BasePreloadingRepo {
    constructor(reqBuilder){
        this.idMap = new Map();

        this.reqBuilder = reqBuilder;

        let that = this;
        let initMap = function(data) {
            for(const item of data){
                that.idMap.set(item.id, item);
            }
        };
        this.dataPromise = new Promise((resolve, reject) => {
            axios.get(that.reqBuilder.getList())
                .then(response => {initMap(response.data); resolve();})
                .catch( err => reject(err)); 
        });
    }

    async getList(){
        await this.dataPromise;
        return Array.from(this.idMap.values());
    }

    getListSync(){
        return Array.from(this.idMap.values());
    }

    async getById(id){
        await this.dataPromise;
        return this.idMap.get(id);
    }

    getByIdSync(id){
        return this.idMap.get(id);
    }
}

class ProductTypeRequestBuilder extends BaseRequestBuilder{
    constructor(){
        super('/api/product-types');
    }
}

class ProductTypeRepo extends BasePreloadingRepo {
    constructor(){
        super(new ProductTypeRequestBuilder());
    }    
}

let _productTypeRepo = null;
export function getProductTypeRepo(){
    if(_productTypeRepo == null) _productTypeRepo = new ProductTypeRepo();
    return _productTypeRepo;
}

// Request builder for fetching snapshot info
class SnapReqBuilder extends BaseRequestBuilder {
    constructor(){
        super('/api/snapshots');
    }

    getLast(){
        return `${this.baseUrl}/last`
    }
}

class SnapRepo extends BaseRepo{
    constructor(){
        super(new SnapReqBuilder());
    }

    async getLast(){
        return axiosRespHandler(axios.get(this.reqBuilder.getLast()));
    }
}

let _snapRepo = null;
export function getSnapRepo() {
    if(_snapRepo == null) _snapRepo = new SnapRepo();
    return _snapRepo;
}

class ShopReqBuilder extends BaseRequestBuilder {
    constructor(){
        super('/api/shops');
    }

    getByName(name){
        return `${this.baseUrl}?name=${name}`;
    }
}

class ShopRepo extends BasePreloadingRepo{
    constructor(){
        super(new ShopReqBuilder());
    }

    async getByName(name){
        await this.dataPromise;

        let shops = [];
        let lowerName = name.toLowerCase();
        for(const shop of this.idMap.values()){
            if(shop.name.toLowerCase().includes(lowerName)){
                shops.push(shop);
            }
        }
        return shops;
    }
}

let _shopRepo = null;
export function getShopRepo() {
    if(_shopRepo == null) _shopRepo = new ShopRepo();
    return _shopRepo;
}

class WeightProductQuery extends BaseRequestBuilder {
    constructor(){
        super('/api/weight-products');
    }
}

class WeightProductRepo extends BaseRepo {
    constructor(){
        super(new WeightProductQuery());
    }
}

let _weightProductRepo = null;
export function getWeightProductRepo(){
    if(_weightProductRepo == null) _weightProductRepo = new WeightProductRepo();
    return _weightProductRepo;
}


class VolumeProductQuery extends BaseRequestBuilder {
    constructor(){
        super('/api/volume-products');
    }
}

class VolumeProductRepo extends BaseRepo {
    constructor(){
        super(new VolumeProductQuery());
    }
}

let _volumeProductRepo = null;
export function getVolumeProductRepo(){
    if(_volumeProductRepo == null) _volumeProductRepo = new VolumeProductRepo();
    return _volumeProductRepo;
}