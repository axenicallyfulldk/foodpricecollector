package volumeproduct

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"web/internal/interfaces"
	"web/internal/types"

	"go.uber.org/zap"
)

type VolumeProductPostgresRepo struct {
	db     *sql.DB
	logger *zap.SugaredLogger
}

func NewVolumeProductPostgresRepo(db *sql.DB, logger *zap.SugaredLogger) *VolumeProductPostgresRepo {
	return &VolumeProductPostgresRepo{db: db, logger: logger}
}

const getByID = `SELECT id, snapshot_id, type, subtype, name, volume, price, shop_id, url FROM volume_product WHERE id = $1 LIMIT 1`

func (r *VolumeProductPostgresRepo) GetByID(ctx context.Context, id int) (*types.VolumeProduct, error) {
	row := r.db.QueryRowContext(ctx, getByID, id)
	var product types.VolumeProduct
	err := row.Scan(&product.ID, &product.SnapID, &product.Type, &product.Subtype, &product.Name, &product.Volume, &product.Price, &product.ShopID, &product.URL)
	return &product, err
}

func fetchProductsByQuery(ctx context.Context, db *sql.DB, query string) ([]*types.VolumeProduct, error) {
	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var products = make([]*types.VolumeProduct, 0)
	for rows.Next() {
		var product types.VolumeProduct
		if err := rows.Scan(&product.ID, &product.SnapID, &product.Type, &product.Subtype, &product.Name, &product.Volume, &product.Price, &product.ShopID, &product.URL); err != nil {
			return nil, err
		}
		products = append(products, &product)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return products, nil
}

const getAll = `SELECT id, snapshot_id, type, subtype, name, volume, price, shop_id, url FROM volume_product`

func (r *VolumeProductPostgresRepo) GetAll(ctx context.Context) ([]*types.VolumeProduct, error) {
	return fetchProductsByQuery(ctx, r.db, getAll)
}

const nameLike = "lower(name) like '%%%s%%'"

func queryToCondition(query interfaces.IVolumeProductQuery) string {
	var condStrs []string
	if substrs, has := query.GetNameSubstrings(); has {
		// TODO: make escaping
		var likes []string
		for _, substr := range substrs {
			likes = append(likes, fmt.Sprintf(nameLike, strings.ToLower(substr)))
		}
		condStrs = append(condStrs, fmt.Sprintf("(%s)", strings.Join(likes, " or ")))
	}

	if minPrice, has := query.GetMinPrice(); has {
		condStrs = append(condStrs, fmt.Sprintf("price >= %f", minPrice))
	}

	if maxPrice, has := query.GetMaxPrice(); has {
		condStrs = append(condStrs, fmt.Sprintf("price <= %f", maxPrice))
	}

	if minPricePerLiter, has := query.GetMinPricePerLiter(); has {
		condStrs = append(condStrs, fmt.Sprintf("10*price/volume >= %f", minPricePerLiter))
	}

	if maxPricePerLiter, has := query.GetMaxPricePerLiter(); has {
		condStrs = append(condStrs, fmt.Sprintf("10*price/volume <= %f", maxPricePerLiter))
	}

	if types, has := query.GetTypeIn(); has {
		typesStr := strings.Trim(strings.Replace(fmt.Sprint(types), " ", ",", -1), "[]")
		condStrs = append(condStrs, fmt.Sprintf("type in (%s)", typesStr))
	}

	if shop, has := query.GetShop(); has {
		condStrs = append(condStrs, fmt.Sprintf("shop_id = %d", shop))
	}

	if snapshot, has := query.GetSnapshot(); has {
		condStrs = append(condStrs, fmt.Sprintf("snapshot_id = %d", snapshot))
	}

	return strings.Join(condStrs, " and ")
}

const getByQuery = `SELECT id, snapshot_id, type, subtype, name, volume, price, shop_id, url FROM volume_product `

func (r *VolumeProductPostgresRepo) GetByQuery(ctx context.Context, query interfaces.IVolumeProductQuery) ([]*types.VolumeProduct, error) {
	queryStr := getByQuery
	whereCondStr := queryToCondition(query)
	if whereCondStr != "" {
		queryStr = fmt.Sprintf("%s WHERE %s", queryStr, whereCondStr)
	}

	if limitOffset, has := query.GetLimitOffset(); has {
		queryStr = fmt.Sprintf("%s LIMIT %d OFFSET %d", queryStr, limitOffset.Limit, limitOffset.Offset)
	}

	r.logger.Infow(queryStr,
		"type", "QUERY",
		"addr", "127.0.0.1")

	return fetchProductsByQuery(ctx, r.db, queryStr)
}

const updateQuery = `UPDATE volume_product SET name=$2, type=$3, subtype=$4, volume=$5, price=$6, shop_id=$7, url=$8, snapshot_id=$9  WHERE id = $1;`

func (r *VolumeProductPostgresRepo) Update(ctx context.Context, product *types.VolumeProduct) error {
	_, err := r.db.ExecContext(ctx, updateQuery, product.ID, product.Name, product.Type, product.Subtype, product.Volume, product.Price, product.ShopID, product.URL, product.SnapID)
	return err
}

const deleteQuery = `DELETE FROM volume_product WHERE id = $1`

func (r *VolumeProductPostgresRepo) Delete(ctx context.Context, id int) error {
	_, err := r.db.ExecContext(ctx, deleteQuery, id)
	return err
}
