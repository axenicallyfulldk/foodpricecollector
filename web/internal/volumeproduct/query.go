package volumeproduct

/*
	This file defines VolumeProductQuery that implements IVolumeProductQuery
	interface.
*/

import "web/internal/types"

type VolumeProductQuery struct {
	nameSubstrings   []string
	minPrice         *float32
	maxPrice         *float32
	minPricePerLiter *float32
	maxPricePerLiter *float32
	types            []types.ProductTypeID
	limitOffset      *types.LimitOffset
	shopID           *int
	snapshotID       *int
}

func (q *VolumeProductQuery) GetNameSubstrings() ([]string, bool) {
	return q.nameSubstrings, len(q.nameSubstrings) > 0
}

func (q *VolumeProductQuery) AddNameSubstring(substr string) {
	q.nameSubstrings = append(q.nameSubstrings, substr)
}

func (q *VolumeProductQuery) SetNameSubstrings(substrs []string) {
	q.nameSubstrings = substrs
}

func (q *VolumeProductQuery) GetMinPrice() (float32, bool) {
	if q.minPrice == nil {
		return 0, false
	}

	return *q.minPrice, true
}

func (q *VolumeProductQuery) SetMinPrice(minPrice float32) {
	q.minPrice = new(float32)
	*q.minPrice = minPrice
}

func (q *VolumeProductQuery) GetMaxPrice() (float32, bool) {
	if q.maxPrice == nil {
		return 0, false
	}

	return *q.maxPrice, true
}

func (q *VolumeProductQuery) SetMaxPrice(maxPrice float32) {
	q.maxPrice = new(float32)
	*q.maxPrice = maxPrice
}

func (q *VolumeProductQuery) GetMinPricePerLiter() (float32, bool) {
	if q.minPricePerLiter == nil {
		return 0, false
	}

	return *q.minPricePerLiter, true
}

func (q *VolumeProductQuery) SetMinPricePerLiter(minPricePerLiter float32) {
	q.minPricePerLiter = new(float32)
	*q.minPricePerLiter = minPricePerLiter
}

func (q *VolumeProductQuery) GetMaxPricePerLiter() (float32, bool) {
	if q.maxPricePerLiter == nil {
		return 0, false
	}

	return *q.maxPricePerLiter, true
}

func (q *VolumeProductQuery) SetMaxPricePerLiter(maxPricePerLiter float32) {
	q.maxPricePerLiter = new(float32)
	*q.maxPricePerLiter = maxPricePerLiter
}

func (q *VolumeProductQuery) GetTypeIn() ([]types.ProductTypeID, bool) {
	return q.types, len(q.types) > 0
}

func (q *VolumeProductQuery) AddType(t types.ProductTypeID) {
	q.types = append(q.types, t)
}

func (q *VolumeProductQuery) SetTypes(ts []types.ProductTypeID) {
	q.types = ts
}

func (q *VolumeProductQuery) GetLimitOffset() (types.LimitOffset, bool) {
	if q.limitOffset == nil {
		return types.LimitOffset{}, false
	}

	return *q.limitOffset, true
}

func (q *VolumeProductQuery) SetLimitOffset(limitOffset types.LimitOffset) {
	q.limitOffset = &types.LimitOffset{}
	*q.limitOffset = limitOffset
}

func (q *VolumeProductQuery) GetShop() (int, bool) {
	if q.shopID == nil {
		return 0, false
	}

	return *q.shopID, true
}

func (q *VolumeProductQuery) SetShop(shopID int) {
	q.shopID = new(int)
	*q.shopID = shopID
}

func (q *VolumeProductQuery) GetSnapshot() (int, bool) {
	if q.snapshotID == nil {
		return 0, false
	}

	return *q.snapshotID, true
}

func (q *VolumeProductQuery) SetSnapshot(snapshotID int) {
	q.snapshotID = new(int)
	*q.snapshotID = snapshotID
}
