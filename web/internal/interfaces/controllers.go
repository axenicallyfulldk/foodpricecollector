package interfaces

import "github.com/gin-gonic/gin"

type IProductTypeController interface {
	// Returns all product types
	Index(gc *gin.Context)
}

type ISnapshotController interface {
	// Returns all snapshots
	Index(gc *gin.Context)
	// Returns last snapshot
	Last(gc *gin.Context)
	// Returns specific snapshot
	Show(gc *gin.Context)
	// Updates snapshot data
	Update(gc *gin.Context)
	// Deletes snapshot
	Destroy(gc *gin.Context)
}

type IShopController interface {
	Index(gc *gin.Context)
	Show(gc *gin.Context)
	Update(gc *gin.Context)
	Destroy(gc *gin.Context)
}

type IWeightProductController interface {
	Index(gc *gin.Context)
	Show(gc *gin.Context)
	Update(gc *gin.Context)
	Destroy(gc *gin.Context)
}

type IVolumeProductController interface {
	Index(gc *gin.Context)
	Show(gc *gin.Context)
	Update(gc *gin.Context)
	Destroy(gc *gin.Context)
}
