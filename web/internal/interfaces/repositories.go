package interfaces

import (
	"context"
	"time"
	"web/internal/types"
)

type IProductTypeRepo interface {
	GetAll(ctx context.Context) ([]*types.ProductType, error)
}

type IShopRepo interface {
	GetByID(ctx context.Context, id int) (*types.Shop, error)
	GetAll(ctx context.Context) ([]*types.Shop, error)
	GetByNameSubstr(ctx context.Context, name string) ([]*types.Shop, error)
	Update(ctx context.Context, product *types.Shop) error
	Delete(ctx context.Context, id int) error
}

type ISnapshotRepo interface {
	GetByID(ctx context.Context, id int) (*types.Snapshot, error)
	GetAll(ctx context.Context) ([]*types.Snapshot, error)
	GetByDate(ctx context.Context, date time.Time) ([]*types.Snapshot, error)
	GetLast(ctx context.Context) (*types.Snapshot, error)
	Update(ctx context.Context, product *types.Snapshot) error
	Delete(ctx context.Context, id int) error
}

type IWeightProductQuery interface {
	// Product's name has to contain at least one of the given strings
	GetNameSubstrings() ([]string, bool)
	AddNameSubstring(string)
	SetNameSubstrings([]string)
	// Product's price has to be greater or equal
	GetMinPrice() (float32, bool)
	SetMinPrice(float32)
	// Product's price has to be less or equal
	GetMaxPrice() (float32, bool)
	SetMaxPrice(float32)
	// Product's price per 100g has to be greater or equal
	GetMinPricePer100g() (float32, bool)
	SetMinPricePer100g(float32)
	// Product's price per 100g has to be less or equal
	GetMaxPricePer100g() (float32, bool)
	SetMaxPricePer100g(float32)
	// Product has to belong to one of the given types
	GetTypeIn() ([]types.ProductTypeID, bool)
	AddType(types.ProductTypeID)
	SetTypes([]types.ProductTypeID)
	// Limit and offset for pagination
	GetLimitOffset() (types.LimitOffset, bool)
	SetLimitOffset(types.LimitOffset)
	// Shop id
	GetShop() (int, bool)
	SetShop(int)
	// Snapshot id
	GetSnapshot() (int, bool)
	SetSnapshot(int)
}

type IWeightProductRepo interface {
	GetByID(ctx context.Context, id int) (*types.WeightProduct, error)
	GetAll(ctx context.Context) ([]*types.WeightProduct, error)
	GetByQuery(ctx context.Context, query IWeightProductQuery) ([]*types.WeightProduct, error)
	Update(ctx context.Context, product *types.WeightProduct) error
	Delete(ctx context.Context, id int) error
}

type IWeightProductFactory interface {
	GetQuery() IWeightProductRepo
	GetRepo() IWeightProductRepo
}

type IVolumeProductQuery interface {
	// Product's name has to contain at least one of the given strings
	GetNameSubstrings() ([]string, bool)
	AddNameSubstring(string)
	SetNameSubstrings([]string)
	// Product's price has to be greater or equal
	GetMinPrice() (float32, bool)
	SetMinPrice(float32)
	// Product's price has to be less or equal
	GetMaxPrice() (float32, bool)
	SetMaxPrice(float32)
	// Product's price per 100g has to be greater or equal
	GetMinPricePerLiter() (float32, bool)
	SetMinPricePerLiter(float32)
	// Product's price per 100g has to be less or equal
	GetMaxPricePerLiter() (float32, bool)
	SetMaxPricePerLiter(float32)
	// Product has to belong to one of the given types
	GetTypeIn() ([]types.ProductTypeID, bool)
	AddType(types.ProductTypeID)
	SetTypes([]types.ProductTypeID)
	// Limit and offset for pagination
	GetLimitOffset() (types.LimitOffset, bool)
	SetLimitOffset(types.LimitOffset)
	// Shop id
	GetShop() (int, bool)
	SetShop(int)
	// Snapshot id
	GetSnapshot() (int, bool)
	SetSnapshot(int)
}

type IVolumeProductRepo interface {
	GetByID(ctx context.Context, id int) (*types.VolumeProduct, error)
	GetAll(ctx context.Context) ([]*types.VolumeProduct, error)
	GetByQuery(ctx context.Context, query IVolumeProductQuery) ([]*types.VolumeProduct, error)
	Update(ctx context.Context, product *types.VolumeProduct) error
	Delete(ctx context.Context, id int) error
}

type IVolumeProductFactory interface {
	GetQuery() IVolumeProductRepo
	GetRepo() IVolumeProductRepo
}
