package types

type ProductTypeID int

type ProductType struct {
	ID   ProductTypeID `json:"id"`
	Name string        `json:"name"`
}

type WeightProduct struct {
	ID      int           `json:"id"`
	SnapID  int           `json:"snapshot"`
	Type    ProductTypeID `json:"type"`
	Subtype NullInt64     `json:"subtype"`
	Name    string        `json:"name"`
	Weight  float32       `json:"weight"`
	Price   float32       `json:"price"`
	ShopID  int           `json:"shop"`
	URL     NullString    `json:"url"`
}

type VolumeProduct struct {
	ID      int           `json:"id"`
	SnapID  int           `json:"snapshot"`
	Type    ProductTypeID `json:"type"`
	Subtype NullInt64     `json:"subtype"`
	Name    string        `json:"name"`
	Volume  float32       `json:"volume"`
	Price   float32       `json:"price"`
	ShopID  int           `json:"shop"`
	URL     NullString    `json:"url"`
}
