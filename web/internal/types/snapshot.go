package types

import "time"

type Snapshot struct {
	ID   int       `json:"id"`
	Date time.Time `json:"date"`
}
