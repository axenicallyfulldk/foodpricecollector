package types

import "encoding/json"

type LimitOffset struct {
	Limit  int
	Offset int
}

const maxUint = ^uint(0)
const minUint = 0
const maxInt = int(maxUint >> 1)
const minInt = -maxInt - 1

func NewLimitOffset() LimitOffset {
	return LimitOffset{
		Limit:  maxInt,
		Offset: 0,
	}
}

type APIError struct {
	Status    int    `json:"status"`
	Title     string `json:"title"`
	Placement string `json:"placement"`
	Field     string `json:"field"`
}

func NewAPIError(status int, title string, placement string, field string) *APIError {
	return &APIError{Status: status, Title: title, Placement: placement, Field: field}
}

func (err *APIError) MarshalJSON() ([]byte, error) {
	if err.Placement == "general" {
		// If placement general we have to hide field
		return json.Marshal(&struct {
			Status    int    `json:"status"`
			Title     string `json:"title"`
			Placement string `json:"placement"`
		}{
			Status:    err.Status,
			Title:     err.Title,
			Placement: err.Placement,
		})
	}

	return json.Marshal(*err)
}

type APIErrResp struct {
	Errors []*APIError `json:"errors"`
}

func NewAPIErrResp() *APIErrResp {
	return &APIErrResp{}
}

func (errResponse *APIErrResp) AddError(status int, title string, placement string, field string) {
	errResponse.Errors = append(errResponse.Errors, NewAPIError(status, title, placement, field))
}

func NewAPIErrRespWithErr(status int, title string, placement string, field string) *APIErrResp {
	errResp := NewAPIErrResp()
	errResp.AddError(status, title, placement, field)
	return errResp
}

func NewAPIErrRespFromErr(status int, err error) *APIErrResp {
	errResp := NewAPIErrResp()
	errResp.AddError(status, err.Error(), "general", "")
	return errResp
}
