package controllers

import (
	"context"
	"net/http"
	"net/url"
	"strconv"
	"web/internal/interfaces"
	"web/internal/types"
	"web/internal/weightproduct"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type WeightProductController struct {
	weightProductRepo interfaces.IWeightProductRepo
	logger            *zap.SugaredLogger
}

func NewWeightProductController(wpr interfaces.IWeightProductRepo, logger *zap.SugaredLogger) *WeightProductController {
	return &WeightProductController{weightProductRepo: wpr, logger: logger}
}

func prepareWeightQuery(params url.Values) (interfaces.IWeightProductQuery, *types.APIErrResp) {
	query := &weightproduct.WeightProductQuery{}
	if names, has := params["name"]; has {
		for _, name := range names {
			query.AddNameSubstring(name)
		}
	}

	if minPriceStr, has := params["minPrice"]; has {
		minPrice, err := strconv.ParseFloat(minPriceStr[0], 32)
		if err != nil {
			return nil, types.NewAPIErrRespWithErr(http.StatusBadRequest, err.Error(), "field", "minPrice")
		}
		query.SetMinPrice(float32(minPrice))
	}

	if maxPriceStr, has := params["maxPrice"]; has {
		maxPrice, err := strconv.ParseFloat(maxPriceStr[0], 32)
		if err != nil {
			return nil, types.NewAPIErrRespWithErr(http.StatusBadRequest, err.Error(), "field", "maxPrice")
		}
		query.SetMaxPrice(float32(maxPrice))
	}

	if minPricePer100gStr, has := params["minPricePer100g"]; has {
		minPricePer100g, err := strconv.ParseFloat(minPricePer100gStr[0], 32)
		if err != nil {
			return nil, types.NewAPIErrRespWithErr(http.StatusBadRequest, err.Error(), "field", "minPricePer100g")
		}
		query.SetMinPricePer100g(float32(minPricePer100g))
	}

	if maxPricePer100gStr, has := params["maxPricePer100g"]; has {
		maxPricePer100g, err := strconv.ParseFloat(maxPricePer100gStr[0], 32)
		if err != nil {
			return nil, types.NewAPIErrRespWithErr(http.StatusBadRequest, err.Error(), "field", "maxPricePer100g")
		}
		query.SetMaxPricePer100g(float32(maxPricePer100g))
	}

	if typesStr, has := params["type"]; has {
		for _, typeStr := range typesStr {
			ptype, err := strconv.ParseInt(typeStr, 10, 8)
			if err != nil {
				return nil, types.NewAPIErrRespWithErr(http.StatusBadRequest, err.Error(), "field", "type")
			}
			query.AddType(types.ProductTypeID(ptype))
		}
	}

	_, hasLimit := params["limit"]
	_, hasOffset := params["offset"]

	if hasLimit || hasOffset {
		var limitOffset = types.NewLimitOffset()
		if hasLimit {
			limitStr, _ := params["limit"]
			limit, err := strconv.ParseInt(limitStr[0], 10, 64)
			if err != nil {
				return nil, types.NewAPIErrRespWithErr(http.StatusBadRequest, err.Error(), "field", "limit")
			}
			limitOffset.Limit = int(limit)
		}

		if hasOffset {
			offsetStr, _ := params["offset"]
			offset, err := strconv.ParseInt(offsetStr[0], 10, 64)
			if err != nil {
				return nil, types.NewAPIErrRespWithErr(http.StatusBadRequest, err.Error(), "field", "offset")
			}
			limitOffset.Offset = int(offset)
		}

		query.SetLimitOffset(limitOffset)
	}

	if shopStr, has := params["shop"]; has {
		shop, err := strconv.ParseInt(shopStr[0], 10, 64)
		if err != nil {
			return nil, types.NewAPIErrRespWithErr(http.StatusBadRequest, err.Error(), "field", "shop")
		}
		query.SetShop(int(shop))
	}

	if snapshotStr, has := params["snapshot"]; has {
		snapshot, err := strconv.ParseInt(snapshotStr[0], 10, 64)
		if err != nil {
			return nil, types.NewAPIErrRespWithErr(http.StatusBadRequest, err.Error(), "field", "snapshot")
		}
		query.SetSnapshot(int(snapshot))
	}

	return query, nil
}

func (c *WeightProductController) Index(gc *gin.Context) {
	query, apiErr := prepareWeightQuery(gc.Request.URL.Query())
	if apiErr != nil {
		gc.JSON(http.StatusBadRequest, apiErr)
		return
	}

	products, err := c.weightProductRepo.GetByQuery(context.Background(), query)
	if err != nil {
		gc.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	gc.JSON(http.StatusOK, products)
}

func (c *WeightProductController) Show(gc *gin.Context) {
	panic("not implemented") // TODO: Implement
}

func (c *WeightProductController) Update(gc *gin.Context) {
	panic("not implemented") // TODO: Implement
}

func (c *WeightProductController) Destroy(gc *gin.Context) {
	panic("not implemented") // TODO: Implement
}
