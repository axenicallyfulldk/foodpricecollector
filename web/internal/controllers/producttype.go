package controllers

import (
	"context"
	"net/http"
	"web/internal/interfaces"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type ProductTypeController struct {
	ptRepo interfaces.IProductTypeRepo
	logger *zap.SugaredLogger
}

func NewProductTypeController(ptRepo interfaces.IProductTypeRepo,
	logger *zap.SugaredLogger) *ProductTypeController {
	return &ProductTypeController{ptRepo: ptRepo, logger: logger}
}

// Returns all product types
func (c *ProductTypeController) Index(gc *gin.Context) {
	res, _ := c.ptRepo.GetAll(context.Background())
	gc.JSON(http.StatusOK, res)
}
