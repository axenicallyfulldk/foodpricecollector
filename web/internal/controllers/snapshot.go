package controllers

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"web/internal/interfaces"
	"web/internal/types"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type SnapshotController struct {
	snapRepo interfaces.ISnapshotRepo
	logger   *zap.SugaredLogger
}

func NewSnapshotController(snapRepo interfaces.ISnapshotRepo, logger *zap.SugaredLogger) *SnapshotController {
	return &SnapshotController{snapRepo: snapRepo, logger: logger}
}

func (c *SnapshotController) Index(gc *gin.Context) {
	snaps, _ := c.snapRepo.GetAll(context.Background())
	gc.JSON(http.StatusOK, snaps)
}

func (c *SnapshotController) Show(gc *gin.Context) {
	snapIDStr := gc.Param("id")
	snapID, err := strconv.ParseInt(snapIDStr, 10, 32)
	if err != nil {
		errResp := types.NewAPIErrRespWithErr(400, fmt.Sprintf("Snap id \"%s\" is incorrect!", snapIDStr), "general", "")
		gc.JSON(http.StatusBadRequest, errResp)
		return
	}

	snap, err := c.snapRepo.GetByID(context.Background(), int(snapID))
	if err != nil {
		gc.JSON(http.StatusInternalServerError, types.NewAPIErrRespFromErr(http.StatusInternalServerError, err))
		return
	}
	gc.JSON(http.StatusOK, snap)
}

func (c *SnapshotController) Last(gc *gin.Context) {
	snap, _ := c.snapRepo.GetLast(context.Background())
	gc.JSON(http.StatusOK, snap)
}

func (c *SnapshotController) Update(gc *gin.Context) {
	panic("not implemented") // TODO: Implement
}

func (c *SnapshotController) Destroy(gc *gin.Context) {
	panic("not implemented") // TODO: Implement
}
