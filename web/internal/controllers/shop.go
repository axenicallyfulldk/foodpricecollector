package controllers

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"web/internal/interfaces"
	"web/internal/types"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type ShopController struct {
	shopRepo interfaces.IShopRepo
	logger   *zap.SugaredLogger
}

func NewShopController(shopRepo interfaces.IShopRepo, logger *zap.SugaredLogger) *ShopController {
	return &ShopController{shopRepo: shopRepo, logger: logger}
}

func (c *ShopController) Index(gc *gin.Context) {
	shops, err := c.shopRepo.GetAll(context.Background())
	if err != nil {
		gc.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	gc.JSON(http.StatusOK, shops)
}

func (c *ShopController) Show(gc *gin.Context) {
	shopIDStr := gc.Param("id")
	shopID, err := strconv.ParseInt(shopIDStr, 10, 32)
	if err != nil {
		errResp := types.NewAPIErrRespWithErr(400, fmt.Sprintf("Shop id \"%s\" is incorrect!", shopIDStr), "general", "")
		gc.JSON(http.StatusBadRequest, errResp)
		return
	}

	shop, err := c.shopRepo.GetByID(context.Background(), int(shopID))
	if err != nil {
		gc.JSON(http.StatusInternalServerError, types.NewAPIErrRespFromErr(http.StatusInternalServerError, err))
		return
	}
	gc.JSON(http.StatusOK, shop)
}

func (c *ShopController) Update(gc *gin.Context) {
	panic("not implemented") // TODO: Implement
}

func (c *ShopController) Destroy(gc *gin.Context) {
	panic("not implemented") // TODO: Implement
}
