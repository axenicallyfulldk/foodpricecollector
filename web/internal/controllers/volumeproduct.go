package controllers

import (
	"context"
	"net/http"
	"net/url"
	"strconv"
	"web/internal/interfaces"
	"web/internal/types"
	"web/internal/volumeproduct"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type VolumeProductController struct {
	volumeProductRepo interfaces.IVolumeProductRepo
	logger            *zap.SugaredLogger
}

func NewVolumeProductController(vpr interfaces.IVolumeProductRepo, logger *zap.SugaredLogger) *VolumeProductController {
	return &VolumeProductController{volumeProductRepo: vpr, logger: logger}
}

func prepareVolumeQuery(params url.Values) (interfaces.IVolumeProductQuery, error) {
	query := &volumeproduct.VolumeProductQuery{}
	if names, has := params["name"]; has {
		for _, name := range names {
			query.AddNameSubstring(name)
		}
	}

	if minPriceStr, has := params["minPrice"]; has {
		minPrice, err := strconv.ParseFloat(minPriceStr[0], 32)
		if err != nil {
			return nil, err
		}
		query.SetMinPrice(float32(minPrice))
	}

	if maxPriceStr, has := params["maxPrice"]; has {
		maxPrice, err := strconv.ParseFloat(maxPriceStr[0], 32)
		if err != nil {
			return nil, err
		}
		query.SetMaxPrice(float32(maxPrice))
	}

	if minPricePerLiterStr, has := params["minPricePerLiter"]; has {
		minPricePerLiter, err := strconv.ParseFloat(minPricePerLiterStr[0], 32)
		if err != nil {
			return nil, err
		}
		query.SetMinPricePerLiter(float32(minPricePerLiter))
	}

	if maxPricePerLiterStr, has := params["maxPricePerLiter"]; has {
		maxPricePerLiter, err := strconv.ParseFloat(maxPricePerLiterStr[0], 32)
		if err != nil {
			return nil, err
		}
		query.SetMaxPricePerLiter(float32(maxPricePerLiter))
	}

	if typesStr, has := params["type"]; has {
		for _, typeStr := range typesStr {
			ptype, err := strconv.ParseInt(typeStr, 10, 8)
			if err != nil {
				return nil, err
			}
			query.AddType(types.ProductTypeID(ptype))
		}
	}

	_, hasLimit := params["limit"]
	_, hasOffset := params["offset"]

	if hasLimit || hasOffset {
		var limitOffset = types.NewLimitOffset()
		if hasLimit {
			limitStr, _ := params["limit"]
			limit, err := strconv.ParseInt(limitStr[0], 10, 64)
			if err != nil {
				return nil, err
			}
			limitOffset.Limit = int(limit)
		}

		if hasOffset {
			offsetStr, _ := params["offset"]
			offset, err := strconv.ParseInt(offsetStr[0], 10, 64)
			if err != nil {
				return nil, err
			}
			limitOffset.Offset = int(offset)
		}

		query.SetLimitOffset(limitOffset)
	}

	if shopStr, has := params["shop"]; has {
		shop, err := strconv.ParseInt(shopStr[0], 10, 64)
		if err != nil {
			return nil, err
		}
		query.SetShop(int(shop))
	}

	if snapshotStr, has := params["snapshot"]; has {
		snapshot, err := strconv.ParseInt(snapshotStr[0], 10, 64)
		if err != nil {
			return nil, err
		}
		query.SetSnapshot(int(snapshot))
	}

	return query, nil
}

func (c *VolumeProductController) Index(gc *gin.Context) {
	query, _ := prepareVolumeQuery(gc.Request.URL.Query())
	products, err := c.volumeProductRepo.GetByQuery(context.Background(), query)
	if err != nil {
		gc.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	gc.JSON(http.StatusOK, products)
}

func (c *VolumeProductController) Show(gc *gin.Context) {
	panic("not implemented") // TODO: Implement
}

func (c *VolumeProductController) Update(gc *gin.Context) {
	panic("not implemented") // TODO: Implement
}

func (c *VolumeProductController) Destroy(gc *gin.Context) {
	panic("not implemented") // TODO: Implement
}
