package weightproduct

/*
	This file defines WeightProductQuery that implements IWeightProductQuery
	interface.
*/

import "web/internal/types"

type WeightProductQuery struct {
	nameSubstrings  []string
	minPrice        *float32
	maxPrice        *float32
	minPricePer100g *float32
	maxPricePer100g *float32
	types           []types.ProductTypeID
	limitOffset     *types.LimitOffset
	shopID          *int
	snapshotID      *int
}

func (q *WeightProductQuery) GetNameSubstrings() ([]string, bool) {
	return q.nameSubstrings, len(q.nameSubstrings) > 0
}

func (q *WeightProductQuery) AddNameSubstring(substr string) {
	q.nameSubstrings = append(q.nameSubstrings, substr)
}

func (q *WeightProductQuery) SetNameSubstrings(substrs []string) {
	q.nameSubstrings = substrs
}

func (q *WeightProductQuery) GetMinPrice() (float32, bool) {
	if q.minPrice == nil {
		return 0, false
	}

	return *q.minPrice, true
}

func (q *WeightProductQuery) SetMinPrice(minPrice float32) {
	q.minPrice = new(float32)
	*q.minPrice = minPrice
}

func (q *WeightProductQuery) GetMaxPrice() (float32, bool) {
	if q.maxPrice == nil {
		return 0, false
	}

	return *q.maxPrice, true
}

func (q *WeightProductQuery) SetMaxPrice(maxPrice float32) {
	q.maxPrice = new(float32)
	*q.maxPrice = maxPrice
}

func (q *WeightProductQuery) GetMinPricePer100g() (float32, bool) {
	if q.minPricePer100g == nil {
		return 0, false
	}

	return *q.minPricePer100g, true
}

func (q *WeightProductQuery) SetMinPricePer100g(minPricePer100g float32) {
	q.minPricePer100g = new(float32)
	*q.minPricePer100g = minPricePer100g
}

func (q *WeightProductQuery) GetMaxPricePer100g() (float32, bool) {
	if q.maxPricePer100g == nil {
		return 0, false
	}

	return *q.maxPricePer100g, true
}

func (q *WeightProductQuery) SetMaxPricePer100g(maxPricePer100g float32) {
	q.maxPricePer100g = new(float32)
	*q.maxPricePer100g = maxPricePer100g
}

func (q *WeightProductQuery) GetTypeIn() ([]types.ProductTypeID, bool) {
	return q.types, len(q.types) > 0
}

func (q *WeightProductQuery) AddType(t types.ProductTypeID) {
	q.types = append(q.types, t)
}

func (q *WeightProductQuery) SetTypes(ts []types.ProductTypeID) {
	q.types = ts
}

func (q *WeightProductQuery) GetLimitOffset() (types.LimitOffset, bool) {
	if q.limitOffset == nil {
		return types.LimitOffset{}, false
	}

	return *q.limitOffset, true
}

func (q *WeightProductQuery) SetLimitOffset(limitOffset types.LimitOffset) {
	q.limitOffset = &types.LimitOffset{}
	*q.limitOffset = limitOffset
}

func (q *WeightProductQuery) GetShop() (int, bool) {
	if q.shopID == nil {
		return 0, false
	}

	return *q.shopID, true
}

func (q *WeightProductQuery) SetShop(shopID int) {
	q.shopID = new(int)
	*q.shopID = shopID
}

func (q *WeightProductQuery) GetSnapshot() (int, bool) {
	if q.snapshotID == nil {
		return 0, false
	}

	return *q.snapshotID, true
}

func (q *WeightProductQuery) SetSnapshot(snapshotID int) {
	q.snapshotID = new(int)
	*q.snapshotID = snapshotID
}
