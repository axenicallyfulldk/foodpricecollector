package weightproduct

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"web/internal/interfaces"
	"web/internal/types"

	"go.uber.org/zap"
)

type WeightProductPostgresRepo struct {
	db     *sql.DB
	logger *zap.SugaredLogger
}

func NewWeightProductPostgresRepo(db *sql.DB, logger *zap.SugaredLogger) *WeightProductPostgresRepo {
	return &WeightProductPostgresRepo{db: db, logger: logger}
}

const getByID = `SELECT id, snapshot_id, type, subtype, name, weight, price, shop_id, url FROM weight_product WHERE id = $1 LIMIT 1`

func (r *WeightProductPostgresRepo) GetByID(ctx context.Context, id int) (*types.WeightProduct, error) {
	row := r.db.QueryRowContext(ctx, getByID, id)
	var product types.WeightProduct
	err := row.Scan(&product.ID, &product.SnapID, &product.Type, &product.Subtype, &product.Name, &product.Weight, &product.Price, &product.ShopID, &product.URL)
	return &product, err
}

func fetchProductsByQuery(ctx context.Context, db *sql.DB, query string) ([]*types.WeightProduct, error) {
	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var products = make([]*types.WeightProduct, 0)
	for rows.Next() {
		var product types.WeightProduct
		if err := rows.Scan(&product.ID, &product.SnapID, &product.Type, &product.Subtype, &product.Name, &product.Weight, &product.Price, &product.ShopID, &product.URL); err != nil {
			return nil, err
		}
		products = append(products, &product)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return products, nil
}

const getAll = `SELECT id, snapshot_id, type, subtype, name, weight, price, shop_id, url FROM weight_product`

func (r *WeightProductPostgresRepo) GetAll(ctx context.Context) ([]*types.WeightProduct, error) {
	return fetchProductsByQuery(ctx, r.db, getAll)
}

const nameLike = "lower(name) like '%%%s%%'"

func queryToCondition(query interfaces.IWeightProductQuery) string {
	var condStrs []string
	if substrs, has := query.GetNameSubstrings(); has {
		// TODO: make escaping
		var likes []string
		for _, substr := range substrs {
			likes = append(likes, fmt.Sprintf(nameLike, strings.ToLower(substr)))
		}
		condStrs = append(condStrs, fmt.Sprintf("(%s)", strings.Join(likes, " or ")))
	}

	if minPrice, has := query.GetMinPrice(); has {
		condStrs = append(condStrs, fmt.Sprintf("price >= %f", minPrice))
	}

	if maxPrice, has := query.GetMaxPrice(); has {
		condStrs = append(condStrs, fmt.Sprintf("price <= %f", maxPrice))
	}

	if minPricePer100g, has := query.GetMinPricePer100g(); has {
		condStrs = append(condStrs, fmt.Sprintf("price/weight >= %f", minPricePer100g))
	}

	if maxPricePer100g, has := query.GetMaxPricePer100g(); has {
		condStrs = append(condStrs, fmt.Sprintf("price/weight <= %f", maxPricePer100g))
	}

	if types, has := query.GetTypeIn(); has {
		typesStr := strings.Trim(strings.Replace(fmt.Sprint(types), " ", ",", -1), "[]")
		condStrs = append(condStrs, fmt.Sprintf("type in (%s)", typesStr))
	}

	if shop, has := query.GetShop(); has {
		condStrs = append(condStrs, fmt.Sprintf("shop_id = %d", shop))
	}

	if snapshot, has := query.GetSnapshot(); has {
		condStrs = append(condStrs, fmt.Sprintf("snapshot_id = %d", snapshot))
	}

	return strings.Join(condStrs, " and ")
}

const getByQuery = `SELECT id, snapshot_id, type, subtype, name, weight, price, shop_id, url FROM weight_product `

func (r *WeightProductPostgresRepo) GetByQuery(ctx context.Context, query interfaces.IWeightProductQuery) ([]*types.WeightProduct, error) {
	queryStr := getByQuery
	whereCondStr := queryToCondition(query)
	if whereCondStr != "" {
		queryStr = fmt.Sprintf("%s WHERE %s", queryStr, whereCondStr)
	}

	if limitOffset, has := query.GetLimitOffset(); has {
		queryStr = fmt.Sprintf("%s LIMIT %d OFFSET %d", queryStr, limitOffset.Limit, limitOffset.Offset)
	}

	r.logger.Infow(queryStr,
		"type", "QUERY",
		"addr", "127.0.0.1")

	return fetchProductsByQuery(ctx, r.db, queryStr)
}

const updateQuery = `UPDATE weight_product SET name=$2, type=$3, subtype=$4, weight=$5, price=$6, shop_id=$7, url=$8, snapshot_id=$9  WHERE id = $1;`

func (r *WeightProductPostgresRepo) Update(ctx context.Context, product *types.WeightProduct) error {
	_, err := r.db.ExecContext(ctx, updateQuery, product.ID, product.Name, product.Type, product.Subtype, product.Weight, product.Price, product.ShopID, product.URL, product.SnapID)
	return err
}

const deleteQuery = `DELETE FROM weight_product WHERE id = $1`

func (r *WeightProductPostgresRepo) Delete(ctx context.Context, id int) error {
	_, err := r.db.ExecContext(ctx, deleteQuery, id)
	return err
}
