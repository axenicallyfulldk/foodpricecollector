package snapshot

import (
	"context"
	"database/sql"
	"time"
	"web/internal/types"

	"go.uber.org/zap"
)

type SnapshotRepo struct {
	db     *sql.DB
	logger *zap.SugaredLogger
}

func NewSnapshotRepo(db *sql.DB, logger *zap.SugaredLogger) *SnapshotRepo {
	return &SnapshotRepo{db: db, logger: logger}
}

func fetchSnapshotsByQuery(ctx context.Context, db *sql.DB, query string) ([]*types.Snapshot, error) {
	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var snaps = make([]*types.Snapshot, 0)
	for rows.Next() {
		var snap types.Snapshot
		if err := rows.Scan(&snap.ID, &snap.Date); err != nil {
			return nil, err
		}
		snaps = append(snaps, &snap)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return snaps, nil
}

const getByIDQuery = "SELECT id, date FROM snapshot WHERE id=$1"

func (r *SnapshotRepo) GetByID(ctx context.Context, id int) (*types.Snapshot, error) {
	row := r.db.QueryRowContext(ctx, getByIDQuery, id)
	var snap types.Snapshot
	err := row.Scan(&snap.ID, &snap.Date)
	return &snap, err
}

const getAllQuery = "SELECT id, date FROM snapshot"

func (r *SnapshotRepo) GetAll(ctx context.Context) ([]*types.Snapshot, error) {
	return fetchSnapshotsByQuery(ctx, r.db, getAllQuery)
}

func (r *SnapshotRepo) GetByDate(ctx context.Context, date time.Time) ([]*types.Snapshot, error) {
	panic("not implemented") // TODO: Implement
}

const getLastQuery = "SELECT id, date FROM snapshot ORDER BY date desc LIMIT 1"

func (r *SnapshotRepo) GetLast(ctx context.Context) (*types.Snapshot, error) {
	row := r.db.QueryRowContext(ctx, getLastQuery)
	var snap types.Snapshot
	err := row.Scan(&snap.ID, &snap.Date)
	return &snap, err
}

func (r *SnapshotRepo) Update(ctx context.Context, snap *types.Snapshot) error {
	panic("not implemented") // TODO: Implement
}

func (r *SnapshotRepo) Delete(ctx context.Context, id int) error {
	panic("not implemented") // TODO: Implement
}
