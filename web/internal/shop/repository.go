package shop

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"web/internal/types"

	"go.uber.org/zap"
)

type ShopRepo struct {
	db     *sql.DB
	logger *zap.SugaredLogger
}

func NewShopRepo(db *sql.DB, logger *zap.SugaredLogger) *ShopRepo {
	return &ShopRepo{db: db, logger: logger}
}

func fetchShopsByQuery(ctx context.Context, db *sql.DB, query string) ([]*types.Shop, error) {
	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var shops = make([]*types.Shop, 0)
	for rows.Next() {
		var shop types.Shop
		if err := rows.Scan(&shop.ID, &shop.Name); err != nil {
			return nil, err
		}
		shops = append(shops, &shop)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return shops, nil
}

const getByIDQuery = "SELECT id, name FROM shop WHERE id = $1"

func (r *ShopRepo) GetByID(ctx context.Context, id int) (*types.Shop, error) {
	row := r.db.QueryRowContext(ctx, getByIDQuery, id)
	var shop types.Shop
	err := row.Scan(&shop.ID, &shop.Name)
	return &shop, err
}

const getAllQuery = "SELECT id, name FROM shop"

func (r *ShopRepo) GetAll(ctx context.Context) ([]*types.Shop, error) {
	return fetchShopsByQuery(ctx, r.db, getAllQuery)
}

const getByNameSubstr = "SELECT id, name FROM shop WHERE lower(name) like '%%%s%%'"

func (r *ShopRepo) GetByNameSubstr(ctx context.Context, name string) ([]*types.Shop, error) {
	query := fmt.Sprintf(getByNameSubstr, strings.ToLower(name))
	return fetchShopsByQuery(ctx, r.db, query)
}

func (r *ShopRepo) Update(ctx context.Context, product *types.Shop) error {
	panic("not implemented") // TODO: Implement
}

func (r *ShopRepo) Delete(ctx context.Context, id int) error {
	panic("not implemented") // TODO: Implement
}
