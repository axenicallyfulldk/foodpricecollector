package producttype

import (
	"context"
	"web/internal/types"

	"go.uber.org/zap"
)

var productTypeMap = map[types.ProductTypeID]*types.ProductType{
	1: &types.ProductType{ID: 1, Name: "meat"},
	2: &types.ProductType{ID: 2, Name: "fish"},
	3: &types.ProductType{ID: 3, Name: "cheese"},
	4: &types.ProductType{ID: 4, Name: "water"},
	5: &types.ProductType{ID: 5, Name: "vegetable"},
	6: &types.ProductType{ID: 6, Name: "fruit"},
	7: &types.ProductType{ID: 7, Name: "milky drink"},
	8: &types.ProductType{ID: 8, Name: "nut"},
}

var productTypesList []*types.ProductType

func init() {
	for _, val := range productTypeMap {
		productTypesList = append(productTypesList, val)
	}
}

type ProductTypeRepo struct {
	logger *zap.SugaredLogger
}

func NewProductTypeRepo(logger *zap.SugaredLogger) *ProductTypeRepo {
	return &ProductTypeRepo{logger: logger}
}

func (r *ProductTypeRepo) GetAll(ctx context.Context) ([]*types.ProductType, error) {
	return productTypesList, nil
}
