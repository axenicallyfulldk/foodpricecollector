module web

go 1.13

require (
	github.com/gin-gonic/gin v1.7.3
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.7.0
	go.uber.org/zap v1.15.0
)
